//package cat.xes;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.net.URISyntaxException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//
//import javax.transaction.Transactional;
//
//import org.jamgo.model.jpa.dao.LanguageDao;
//import org.jamgo.model.jpa.dao.RolesDao;
//import org.jamgo.model.jpa.entity.Language;
//import org.jamgo.model.jpa.entity.Role;
//import org.jamgo.model.jpa.util.PasswordUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import cat.xes.model.Association;
//import cat.xes.model.Entity;
//import cat.xes.model.EntityState;
//import cat.xes.model.XesUser;
//import cat.xes.model.dao.AssociationsDao;
//import cat.xes.model.dao.EntitiesDao;
//import cat.xes.model.dao.EntityStateDao;
//import cat.xes.model.dao.LegalFormsDao;
//import cat.xes.model.dao.TownsDao;
//
//@Component
//public class ProcessEntities {
//
//	public static Map<String, Long> towns = new HashMap<String, Long>();
//	static {
//		towns.put("Barcelona", 128L);
//		towns.put("Vallbona de les Monges", 752L);
//		towns.put("Barberà del Vallés", 251L);
//		towns.put("Terrassa", 269L);
//		towns.put("Montcada i Reixac", 257L);
//		towns.put("Mataró", 180L);
//		towns.put("L´Hospitalet", 110L);
//		towns.put("Mollet del Vallés", 294L);
//		towns.put("Rubí", 262L);
//		towns.put("Badalona", 127L);
//		towns.put("Solsona", 734L);
//		towns.put("Manresa", 76L);
//		towns.put("Prat de Llobregat", 105L);
//		towns.put("Girona", 467L);
//		towns.put("Santa Coloma de Gramenet", 130L);
////		towns.put("Joanetes",);
////		towns.put("Valldoreix",);
////		towns.put("Madrid",);
//		towns.put("Tona", 243L);
////		towns.put("Puigcerver",);
//		towns.put("Mura", 81L);
//		towns.put("Sant Pere de Ribes", 165L);
//		towns.put("Caldes de Montbui", 275L);
//		towns.put("Cervera", 663L);
//		towns.put("Vilafranca del Penedès", 26L);
////		towns.put("Palma de Mallorca",);
//		towns.put("Sabadell", 263L);
////		towns.put("Catarroja",);
//		towns.put("Vilassar de Mar", 197L);
//		towns.put("Tàrrega", 750L);
//		towns.put("Hospitalet de Llobregat", 110L);
//		towns.put("La Nou de Berguedà", 149L);
//		towns.put("Corbera de Llobregat", 102L);
//		towns.put("Santa Coloma de Queralt", 855L);
//		towns.put("Sant Boi de Llobregat", 116L);
//		towns.put("Vilanova i la Geltrú", 167L);
//		towns.put("El Prat del Llobregat", 105L);
//		towns.put("Cervelló", 100L);
////		towns.put("8193",);
//		towns.put("Mollet del Vallès", 294L);
//		towns.put("Castellar del Vallés", 252L);
//		towns.put("Alella", 168L);
//		towns.put("Prades de La Molsosa", 724L);
//		towns.put("Puig-reig", 152L);
//		towns.put("Torredembarra", 932L);
//		towns.put("El Vendrell", 842L);
//		towns.put("El Prat de Llobregat", 105L);
//	}
//
//	private List<String> nifs = new ArrayList<String>(java.util.Arrays.asList("F08638322", "F08732588", "F08880924", "F25014176", "F58398496", "F61567657", "F62197504", "F64077274", "F65343287", "F65378655", "F65792863", "F65906919", "F65964413", "F65964462", "F66133679", "F66136656", "F66222654", "F66231440", "F66383985", "F66395302", "F66423179", "F66462888", "F66497801", "F66558263", "F66626177", "F66633967", "F66668385", "F66674672", "F66694100", "F66860560", "F66863507", "G55624811", "G55673206",
//		"G58327289", "G59335166", "G59546556", "G60437761", "G61749297", "G61813200", "G62015789", "G62831581", "G63050447", "G63425755", "G63952915", "G64106347", "G64564057", "G64595838", "G64770845", "G64971229", "G65532418", "G65628364", "G65738791", "G65978348", "G66146218", "G66213406", "G66310343", "G66328709", "G66431131", "G66565755", "G66823659", "G86377512", "G96284971", "J25635541", "J62616347", "J66045907", "Q0818002H", "V60636693"));
//
//	@Autowired
//	private EntitiesDao entityDao;
//	@Autowired
//	private TownsDao townsDao;
//	@Autowired
//	private LegalFormsDao legalFormsDao;
////	@Autowired
////	private SectorsDao sectorsDao;
//	@Autowired
//	private RolesDao rolesDao;
//	@Autowired
//	private LanguageDao languagesDao;
//	@Autowired
//	private AssociationsDao associationsDao;
//	@Autowired
//	private EntityStateDao entityStateDao;
//
//	private Role userRole;
//	private Language catalan;
//
//	@Transactional
//	public void process() {
//		Association xes = this.associationsDao.findOne(Association.XES_ID);
////		List<String> comarcas = new ArrayList<String>();
////		List<String> poblacions = new ArrayList<String>();
////
//		this.userRole = this.rolesDao.findOne(2L);
//		this.catalan = this.languagesDao.findOne(1L);
//		EntityState registered = this.entityStateDao.findOne(EntityState.State.Registered.getId());
//
//		FileReader reader = null;
//		BufferedReader bReader = null;
//		try {
//			StringBuffer entityNifs = new StringBuffer("(");
//			URL url = Thread.currentThread().getContextClassLoader().getResource("membre.csv");
//			reader = new FileReader(new File(url.toURI()));
//			bReader = new BufferedReader(reader);
//			String line = null;
//			bReader.readLine();
//			while ((line = bReader.readLine()) != null) {
////				System.out.println(line);
//				String[] parts = line.split("\\|");
////				String codigo = parts[0];
//				String nombre = parts[1];
//				String abrevi = parts[2];
//				String consti = parts[3];
//				String descas = parts[4];
//				String descat = parts[5];
//				String puesto = parts[6];
//				String domici = parts[7];
//				String postal = parts[8];
//				String locali = parts[9];
////				String comarc = parts[10]; // just town is necessary
//				String telefo = parts[11];
//				String urlweb = parts[12];
//				String emilio = parts[13];
//				String connom = parts[14];
//				String contel = parts[15];
//				String conmai = parts[16];
//				String colect = parts[17]; // forma legal.
////				String secto1 = parts[18];
////				String secto2 = parts[19];
//				String imagen = parts[20];
//				String nifent = parts[21];
//
//				Entity entity = this.entityDao.findByNif(nifent);
//				if (entity == null) {
//					entity = new Entity();
//					entity.setName(nombre);
//					entity.setShortName(abrevi);
//					entity.setAddress(domici);
//					entity.setCp(postal);
//					entity.setEmail(emilio);
//					entity.setPhone(telefo);
//					entity.setLegalForm(null);
//					entity.setWeb(urlweb);
//					entity.setNif(nifent);
//				}
//				if (entity.getNif().equals("F66136656")) {
//					System.out.println("");
//				}
//				entity.setYear(consti);
//				entity.setPartnerNumber(!puesto.isEmpty() ? Integer.valueOf(puesto) : 0);
//
////				LocalizedString description = new LocalizedString();
////				description.set("es", descas);
////				description.set("ca", descat);
////				entity.setXesDescription(description);
//				entity.setDescription(descat);
//
//				if ((entity.getTown() == null) && towns.containsKey(locali)) {
//					entity.setTown(this.townsDao.findOne(towns.get(locali)));
//				}
//				if (entity.getLegalForm() == null) {
//					long idFormaJuridica = getFormaJuridica(colect);
//					entity.setLegalForm(this.legalFormsDao.findOne(idFormaJuridica));
//				}
//
//				XesUser user = entity.getUser();
//				if (entity.getUser() == null) {
//					user = new XesUser();
//					entity.setUser(user);
//				}
//				if (entity.getUser().getId() == null) {
//					user.setName(connom);
//					user.setPassword(PasswordUtils.generatePassword());
//					user.setUsername(nifent);
//
//				}
//				user.setEmail(conmai);
//				user.setPhone(contel);
//				user.setEnabled(true);
//				user.setRoles(Collections.singletonList(this.userRole));
//				user.setLanguage(this.catalan);
//
//				entity.setXesState(registered);
//				entity.setActive(true);
//
//				if (entity.getAssociations() == null) {
//					entity.setAssociations(new HashSet<Association>());
//				}
//				if (!entity.getAssociations().contains(xes)) {
//					entity.getAssociations().add(xes);
//				}
//
//				this.entityDao.save(entity);
//
////				if (!comarcas.contains(comarc)) {
////					comarcas.add(comarc);
////					System.out.println(comarc);
////				}
////				if (!poblacions.contains(locali)) {
////					poblacions.add(locali);
////					System.out.println(locali);
////				}
//				if (this.nifs.contains(entity.getNif())) {
//					System.out.println(entity.getNif() + " - " + entity.getName() + " - " + entity.getWeb());
//				}
//			}
////			System.out.println(entityNifs.toString());
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (URISyntaxException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			try {
//				if (reader != null) {
//					reader.close();
//				}
//				if (bReader != null) {
//					bReader.close();
//				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	public static Long getFormaJuridica(String idActual) {
//		switch (idActual) {
//			case "1":
//				return 1L;
//			case "2":
//				return 2L;
//			case "3":
//				return 3L;
//			case "4":
//				return 4L;
//			case "5":
//				return 5L;
//			case "6":
//				return 6L;
//			case "7":
//				return 7L;
//			case "8":
//				return 8L;
//			case "9":
//				return 23L;
//			case "10":
//				return 10L;
//			default:
//				return 0L;
//		}
//	}
//}
