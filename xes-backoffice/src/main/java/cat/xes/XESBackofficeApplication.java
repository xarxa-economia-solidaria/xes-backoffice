package cat.xes;

import java.sql.SQLException;

import org.h2.server.web.WebServlet;
import org.h2.tools.Server;
import org.jamgo.BackofficeApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import cat.xes.service.ServiceConfiguration;

@Configuration
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@Import(ServiceConfiguration.class)
@ComponentScan({ "org.jamgo.model.jpa", "cat.xes" }) // Instead of @SpringBootApplication, this is necessary to scan components from other jars
@EntityScan({ "org.jamgo.model.jpa", "cat.xes.model" })
@EnableJpaRepositories({ "org.jamgo.model.jpa", "cat.xes.model.dao" })
public class XESBackofficeApplication extends BackofficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(XESBackofficeApplication.class, args);
	}

	@Override
	@Bean
	public ServletRegistrationBean h2servletRegistration() {
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
		registrationBean.addUrlMappings("/console/*");
		return registrationBean;
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server h2Server() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers");
	}

}
