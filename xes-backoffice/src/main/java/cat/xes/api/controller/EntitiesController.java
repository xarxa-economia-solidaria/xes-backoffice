package cat.xes.api.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cat.xes.model.Entity;
import cat.xes.service.EntityServices;
import cat.xes.service.data.EntityData;
import cat.xes.service.exception.EntityAlreadyExistsException;

//@PreAuthorize("#request.getLocalAddr().equals(#request.getRemoteAddr())")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/entities")
public class EntitiesController {

	@Autowired
	private EntityServices entityServices;

//	@Autowired
//	private ProcessEntities process;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<EntityData> getAll(HttpServletRequest request) {
		return this.entityServices.getAllEntitiesData();
	}

	@RequestMapping(value = "/{nif}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Entity getOne(HttpServletRequest request, @PathVariable("nif") String nif) {
		return this.entityServices.get(nif);
	}

	@RequestMapping(value = "/logo/{nif}", method = RequestMethod.GET)
	public HttpEntity<byte[]> getLogo(HttpServletRequest request, HttpServletResponse response, @PathVariable("nif") String nif) {
		byte[] logo = this.entityServices.getLogo(nif);
		byte[] encodeLogo = Base64.encode(logo);
		try (InputStream inputStream = new ByteArrayInputStream(encodeLogo)) {
			StreamUtils.copy(inputStream, response.getOutputStream());
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
		} catch (IOException e) {
			// handle
			e.printStackTrace();
		}
		return new ResponseEntity<byte[]>(HttpStatus.OK);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8")
	public HttpEntity register(HttpServletRequest request, @RequestBody EntityData entity) {
		try {
			this.entityServices.register(entity);
			return ResponseEntity.ok().build();
		} catch (EntityAlreadyExistsException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Entity already exists " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	// method to insert or update xes entities into balanç social database
//	@RequestMapping(value = "/process", method = RequestMethod.GET, produces = "application/json")
//	public void process(HttpServletRequest request) {
//		this.process.process();
//
//	}

}
