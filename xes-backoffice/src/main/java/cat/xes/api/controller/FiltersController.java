package cat.xes.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.DatasourceServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cat.xes.model.LegalForm;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;

@CrossOrigin
@RestController
@RequestMapping("api/filters")
public class FiltersController {

	@Autowired
	private DatasourceServices dsServices;

	@RequestMapping(value = "/provinces", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Province> getProvinces() {
		try {
			return this.dsServices.getAll(Province.class);
		} catch (RepositoryForClassNotDefinedException e) {
			e.printStackTrace();
			return new ArrayList<Province>();
		}
	}

	@RequestMapping(value = "/regions", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Region> getRegions() {
		try {
			return this.dsServices.getAll(Region.class);
		} catch (RepositoryForClassNotDefinedException e) {
			e.printStackTrace();
			return new ArrayList<Region>();
		}
	}

	@RequestMapping(value = "/towns", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Town> getTowns() {
		try {
			return this.dsServices.getAll(Town.class);
		} catch (RepositoryForClassNotDefinedException e) {
			e.printStackTrace();
			return new ArrayList<Town>();
		}
	}

	@RequestMapping(value = "/sectors", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Sector> getSectors() {
		try {
			return this.dsServices.getAll(Sector.class);
		} catch (RepositoryForClassNotDefinedException e) {
			e.printStackTrace();
			return new ArrayList<Sector>();
		}
	}

	@RequestMapping(value = "/legalForms", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<LegalForm> getLegalForms() {
		try {
			return this.dsServices.getAll(LegalForm.class);
		} catch (RepositoryForClassNotDefinedException e) {
			e.printStackTrace();
			return new ArrayList<LegalForm>();
		}
	}
}
