package cat.xes.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cat.xes.service.RegisterServces;
import cat.xes.service.data.PersonData;
import cat.xes.service.exception.PersonAlreadyExistsException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/person")
public class PersonController {

	@Autowired
	private RegisterServces registerServices;

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8")
	public HttpEntity register(HttpServletRequest request, @RequestBody PersonData personData) {
		try {
			this.registerServices.requestReqistration(personData);
			return ResponseEntity.ok().build();
		} catch (PersonAlreadyExistsException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Person already exists " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}
