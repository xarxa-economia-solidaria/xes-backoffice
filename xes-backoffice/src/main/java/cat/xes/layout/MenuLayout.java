
package cat.xes.layout;

import java.util.ArrayList;
import java.util.List;

import org.jamgo.CrudItemDef;
import org.jamgo.layout.BaseMenuLayout;
import org.jamgo.layout.form.LanguageFormLayout;
import org.jamgo.model.jpa.entity.Language;
import org.jamgo.model.jpa.entity.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cat.xes.layout.form.EntityFormLayout;
import cat.xes.layout.form.LegalFormFormLayout;
import cat.xes.layout.form.PersonFormLayout;
import cat.xes.layout.form.ProvinceFormLayout;
import cat.xes.layout.form.RegionFormLayout;
import cat.xes.layout.form.SectorFormLayout;
import cat.xes.layout.form.TownFormLayout;
import cat.xes.layout.form.admin.PendingEntityRegistrationsLayout;
import cat.xes.layout.form.admin.PendingPersonRegistrationsLayout;
import cat.xes.layout.form.export.EntityExportLayout;
import cat.xes.layout.form.export.PersonExportLayout;
import cat.xes.layout.form.search.EntitySearchFormLayout;
import cat.xes.layout.form.search.PersonSearchFormLayout;
import cat.xes.model.Entity;
import cat.xes.model.LegalForm;
import cat.xes.model.Person;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MenuLayout extends BaseMenuLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	private PendingEntityRegistrationsLayout pendingPersonRegistrationLayout;
	@Autowired
	private PendingPersonRegistrationsLayout pendingEntityRegistrationLayout;

	@Override
	public List<Object[]> getMainFunctionsDef() {
		List<Object[]> mainFunctionsDef = new ArrayList<Object[]>();
		mainFunctionsDef.add(new Object[] { this.messageSource.getMessage("action.main.registrations.entity"), this.pendingPersonRegistrationLayout });
		mainFunctionsDef.add(new Object[] { this.messageSource.getMessage("action.main.registrations.person"), this.pendingEntityRegistrationLayout });
		return mainFunctionsDef;
	}

	@Override
	public List<CrudItemDef<? extends Model>> getCrudItemDefs() {
		// for roles management:
		// if (VaadinService.getCurrentRequest().isUserInRole(Role.ROLE_ADMIN))

		List<CrudItemDef<? extends Model>> crudItemDefs = new ArrayList<CrudItemDef<? extends Model>>();

//		crudItemDefs.add(new CrudItemDef<cat.xes.model.XesUser>(
//			cat.xes.model.XesUser.class,
//			null,
//			this.messageSource.getMessage("table.users"),
//			new String[] {},
//			new String[] { "username", "enabled" }, //columns
//			new String[] { "User name", "Enabled" },
//			XesUserFormLayout.class,
//			null,
//			"user"));

//		crudItemDefs.add(new CrudItemDef<Role>(
//			Role.class,
//			null,
//			this.messageSource.getMessage("table.roles"),
//			new String[] {},
//			new String[] { "rolename", "description" },
//			new String[] { "Role name", "Description" },
//			RoleFormLayout.class,
//			null,
//			"role"));

		crudItemDefs.add(new CrudItemDef<Language>(
			Language.class,
			null,
			this.messageSource.getMessage("table.languages"),
			new String[] {},
			new String[] { "name", "languageCode", "countryCode" },
			new String[] { "Name", "Language Code", "Country Code" },
			LanguageFormLayout.class,
			null,
			"language"));

		crudItemDefs.add(new CrudItemDef<Town>(
			Town.class,
			null,
			this.messageSource.getMessage("table.town"),
			new String[] { "name.defaultText", "region.name.defaultText" },
			new String[] { "name.defaultText", "region.name.defaultText" },
			new String[] { "Name", "Region" },
			TownFormLayout.class,
			null,
			"role"));

		crudItemDefs.add(new CrudItemDef<Region>(
			Region.class,
			null,
			this.messageSource.getMessage("table.region"),
			new String[] { "name.defaultText", "province.name.defaultText" },
			new String[] { "name.defaultText", "province.name.defaultText" },
			new String[] { "Name", "Province" },
			RegionFormLayout.class,
			null,
			"role"));

		crudItemDefs.add(new CrudItemDef<Province>(
			Province.class,
			null,
			this.messageSource.getMessage("table.province"),
			new String[] { "name.defaultText" },
			new String[] { "name.defaultText" },
			new String[] { "Name" },
			ProvinceFormLayout.class,
			null,
			"role"));

		crudItemDefs.add(new CrudItemDef<LegalForm>(
			LegalForm.class,
			null,
			this.messageSource.getMessage("table.legalform"),
			new String[] { "parent.name.defaultText", "name.defaultText" },
			new String[] { "parent.name.defaultText", "name.defaultText" },
			new String[] { "Parent", "Name" },
			LegalFormFormLayout.class,
			null,
			"role"));

		crudItemDefs.add(new CrudItemDef<Sector>(
			Sector.class,
			null,
			this.messageSource.getMessage("table.sector"),
			new String[] { "parent.name.defaultText", "name.defaultText" },
			new String[] { "parent.name.defaultText", "name.defaultText" },
			new String[] { "Parent", "Name" },
			SectorFormLayout.class,
			null,
			"role"));

		crudItemDefs.add(new CrudItemDef<Entity>(
			Entity.class,
			new Object[] { "findByXes", new Class[] { boolean.class }, true },
			this.messageSource.getMessage("table.entities"),
			new String[] { "town.name.defaultText", "town.region.name.defaultText", "legalForm.name.defaultText", "sector.name.defaultText", "legalForm.parent", "sector.parent", "town.region", "town.region.province" },
			new String[] { "nif", "name", "email", "town.name.defaultText", "town.region.name.defaultText", "legalForm.name.defaultText", "sector.name.defaultText" },
			new String[] { "NIF", "Name", "Email", "Town", "Region", "Legal Form", "Sector" },
			EntityFormLayout.class,
			EntitySearchFormLayout.class,
			null,
			EntityExportLayout.class,
			"entity"));

		crudItemDefs.add(new CrudItemDef<Person>(
			Person.class,
			null,
			this.messageSource.getMessage("table.persons"),
			new String[] { "town.name.defaultText", "town.region.name.defaultText", "town.region", "town.region.province" },
			new String[] { "nif", "name", "email", "town.name.defaultText", "town.region.name.defaultText" },
			new String[] { "NIF", "Name", "Email", "Town", "Region" },
			PersonFormLayout.class,
			PersonSearchFormLayout.class,
			null,
			PersonExportLayout.class,
			"person"));

		return crudItemDefs;
	}

}
