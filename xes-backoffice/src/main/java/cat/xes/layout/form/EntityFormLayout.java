package cat.xes.layout.form;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jamgo.layout.form.ModelFormLayout;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.exception.NoClassParentRelationDefinedException;
import org.jamgo.vaadin.ui.JmgFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Property.ValueChangeEvent;
import com.vaadin.v7.data.Property.ValueChangeListener;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.data.util.filter.Compare.Equal;
import com.vaadin.v7.ui.ComboBox;
import com.vaadin.v7.ui.DateField;
import com.vaadin.v7.ui.TextArea;

import cat.xes.model.Entity;
import cat.xes.model.EntityState;
import cat.xes.model.EvaluableType;
import cat.xes.model.LegalForm;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;
import cat.xes.service.EntityServices;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EntityFormLayout extends ModelFormLayout<Entity> {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private EntityServices entityServices;

	private ComboBox mainSector;
	private ComboBox secondarySector;

	private ComboBox secondaryLegalForm;
	private ComboBox mainLegalForm;

	private ComboBox region;
	private ComboBox province;
	private ComboBox town;

	private JmgFileUpload logoField;

	private BeanItemContainer<Province> provinceBeanContainer = new BeanItemContainer<Province>(Province.class);
	private BeanItemContainer<Region> regionsBeanContainer = new BeanItemContainer<Region>(Region.class);
	private BeanItemContainer<Town> townsBeanContainer = new BeanItemContainer<Town>(Town.class);
	private BeanItemContainer<LegalForm> mainLegalFormBeanContainer = new BeanItemContainer<LegalForm>(LegalForm.class);
	private BeanItemContainer<LegalForm> secondaryLegalFormBeanContainer = new BeanItemContainer<LegalForm>(LegalForm.class);
	private BeanItemContainer<Sector> mainSectorContainer = new BeanItemContainer<Sector>(Sector.class);
	private BeanItemContainer<Sector> secondarySectorContainer = new BeanItemContainer<Sector>(Sector.class);
	private BeanItemContainer<EntityState> entityStateContainer = new BeanItemContainer<EntityState>(EntityState.class);

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws IllegalArgumentException, RepositoryForClassNotDefinedException {
		this.formLayout = formLayout;
		this.binder = binder;

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.active"), "xesActive"));
		Resource logoResource = null;
		if (this.model != null) {
			if (this.model.getLogo() != null) {
				logoResource = new StreamResource(new StreamResource.StreamSource() {
					private static final long serialVersionUID = 1L;

					@Override
					public InputStream getStream() {
						return new ByteArrayInputStream(EntityFormLayout.this.model.getLogo());
					}
				}, this.model.getId() + "_logo_" + this.model.getVersion() + ".png");
			} else {
				logoResource = new ThemeResource("images/edit.png");
			}
			this.logoField = new JmgFileUpload(this.messageSource.getMessage("entity.logo"), logoResource);
			formLayout.addComponent(this.logoField);
		}

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.nif"), "nif"));
		binder.getField("nif").setWidth(25, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.name"), "name"));
		binder.getField("name").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.description"), "description", TextArea.class));
		binder.getField("description").setWidth(100, Unit.PERCENTAGE);
		((TextArea) binder.getField("description")).setNullRepresentation("");
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.web"), "web"));
		binder.getField("web").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.cp"), "cp"));
		binder.getField("cp").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.address"), "address"));
		binder.getField("address").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.shortname"), "shortName"));
		binder.getField("shortName").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.year"), "year"));
		binder.getField("year");
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.email"), "email"));
		binder.getField("email").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.phone"), "phone"));
		binder.getField("phone").setWidth(100, Unit.PERCENTAGE);

		this.province = this.getComboBox("entity.province", "name.defaultText", true, this.provinceBeanContainer, 50);
		formLayout.addComponent(this.province);

		this.region = this.getComboBox("entity.region", "name.defaultText", true, this.regionsBeanContainer, 50);
		formLayout.addComponent(this.region);

		this.town = this.getComboBox("entity.town", "name.defaultText", true, this.townsBeanContainer, 50);
		formLayout.addComponent(this.town);
		this.binder.bind(this.town, "town");
		this.region.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					Region region = (Region) event.getProperty().getValue();
					EntityFormLayout.this.townsBeanContainer.removeAllItems();
					if (region != null) {
						List<Town> towns = EntityFormLayout.this.datasourceServices.getAll(Town.class, region);
						EntityFormLayout.this.townsBeanContainer.addAll(towns);
					}
					EntityFormLayout.this.town.setValue(null);
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});
		this.province.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					Province province = (Province) event.getProperty().getValue();
					EntityFormLayout.this.regionsBeanContainer.removeAllItems();
					EntityFormLayout.this.townsBeanContainer.removeAllItems();
					if (province != null) {
						List<Region> regions = EntityFormLayout.this.datasourceServices.getAll(Region.class, province);
						EntityFormLayout.this.regionsBeanContainer.addAll(regions);
					}
					EntityFormLayout.this.region.setValue(null);
					EntityFormLayout.this.town.setValue(null);
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.othertown"), "otherTown"));
		binder.getField("otherTown").setWidth(100, Unit.PERCENTAGE);

		this.mainLegalForm = this.getComboBox("entity.legalform.main", "name.defaultText", true, this.mainLegalFormBeanContainer, 50);
		formLayout.addComponent(this.mainLegalForm);

		this.secondaryLegalForm = this.getComboBox("entity.legalform.secondary", "name.defaultText", true, this.secondaryLegalFormBeanContainer, 50);
		formLayout.addComponent(this.secondaryLegalForm);
		this.binder.bind(this.secondaryLegalForm, "legalForm");
		this.mainLegalForm.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				EntityFormLayout.this.secondaryLegalFormBeanContainer.removeAllContainerFilters();
				if (EntityFormLayout.this.mainLegalForm.getValue() != null) {
					LegalForm legalForm = (LegalForm) EntityFormLayout.this.mainLegalForm.getValue();
					EntityFormLayout.this.secondaryLegalFormBeanContainer.addContainerFilter(new Equal("parent", legalForm));
				} else {
					// add filter to show empty combo
					EntityFormLayout.this.secondaryLegalFormBeanContainer.addContainerFilter(new Equal("id", null));
				}
				EntityFormLayout.this.secondaryLegalForm.setValue(null);
			}
		});

		this.mainSector = this.getComboBox("entity.sector.main", "name.defaultText", true, this.mainSectorContainer, 50);

		formLayout.addComponent(this.mainSector);

		this.secondarySector = this.getComboBox("entity.sector.secondary", "name.defaultText", true, this.secondarySectorContainer, 50);
		formLayout.addComponent(this.secondarySector);
		this.binder.bind(this.secondarySector, "sector");
		this.mainSector.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				EntityFormLayout.this.secondarySectorContainer.removeAllContainerFilters();
				if (EntityFormLayout.this.mainSector.getValue() != null) {
					Sector sector = (Sector) EntityFormLayout.this.mainSector.getValue();
					EntityFormLayout.this.secondarySectorContainer.addContainerFilter(new Equal("parent", sector));
				} else {
					// add filter to show empty combo
					EntityFormLayout.this.secondarySectorContainer.addContainerFilter(new Equal("id", null));
				}
				EntityFormLayout.this.secondarySector.setValue(null);
			}
		});

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.collaborator"), "collaborator"));

		formLayout.addComponent(this.binder.buildAndBind(this.messageSource.getMessage("entity.pamapam"), "pamAPamUrl"));
		this.binder.getField("pamAPamUrl").setWidth(100, Unit.PERCENTAGE);

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.account.owner"), "bankAccountOwner"));
		binder.getField("bankAccountOwner").setWidth(50, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.account.iban"), "bankAccountIban"));
		binder.getField("bankAccountIban").setWidth(50, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.administration.contact"), "administrationContact"));
		binder.getField("administrationContact").setWidth(50, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.date.request"), "xesRegistryRequestDate"));
		((DateField) binder.getField("xesRegistryRequestDate")).setDateFormat("dd/MM/yyyy");
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.date.resolution"), "xesRegistryResolutionDate"));
		((DateField) binder.getField("xesRegistryResolutionDate")).setDateFormat("dd/MM/yyyy");

		ComboBox states = this.getComboBox("entity.state", "name.defaultText", true, this.entityStateContainer, 25);
		formLayout.addComponent(states);
		this.binder.bind(states, "xesState");

		this.update(this.item, this.model);
	}

	@Override
	public void update(Item item, Entity model) {
		super.update(item, model);
		if (model != null) {
			Resource logoResource = null;
			if (this.model.getLogo() != null) {
				StreamResource.StreamSource streamSource = new StreamResource.StreamSource() {
					private static final long serialVersionUID = 1L;

					@Override
					public InputStream getStream() {
						ByteArrayInputStream stream = new ByteArrayInputStream(EntityFormLayout.this.model.getLogo());
						return stream;
					}
				};
				logoResource = new StreamResource(streamSource, model.getId() + "_logo_" + model.getVersion() + ".png");
				this.logoField.setStream(streamSource.getStream());
			} else {
				logoResource = new ThemeResource("images/edit.png");
			}
			this.logoField.setPreview(logoResource);
			Town town = model.getTown();
			if (town != null) {
				Region region = town.getRegion();
				if (region != null) {
					Province province = region.getProvince();
					this.province.setValue(province);
					this.region.setValue(region);
					this.town.setValue(town);
				}
			}

			Sector sector = model.getSector();
			if (sector != null) {
				Sector parentSector = sector.getParent();
				if (parentSector != null) {
					this.mainSector.setValue(parentSector);
					this.secondarySector.setValue(sector);
				}
			}

			LegalForm legalForm = model.getLegalForm();
			if (legalForm != null) {
				LegalForm parentLegalForm = legalForm.getParent();
				if (parentLegalForm != null) {
					this.mainLegalForm.setValue(parentLegalForm);
					this.secondaryLegalForm.setValue(legalForm);
				}
			}
		}
	}

	@Override
	public Entity save() throws Exception {
		this.model.setXes(true);
		this.model.setType(this.entityServices.getEvaluableType(EvaluableType.Type.Entity));
		if (this.logoField.isUploaded()) {
			byte[] logo = IOUtils.toByteArray(this.logoField.getStream());
			this.logoField.getStream().close();
			this.model.setLogo(logo);
		}
		return super.save();
	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException, NoClassParentRelationDefinedException {
		this.provinceBeanContainer.removeAllItems();
		this.provinceBeanContainer.addNestedContainerProperty("name.defaultText");
		this.provinceBeanContainer.addAll(this.datasourceServices.getAll(Province.class));

		this.mainLegalFormBeanContainer.removeAllItems();
		this.mainLegalFormBeanContainer.addNestedContainerProperty("name.defaultText");
		this.mainLegalFormBeanContainer.addAll(this.datasourceServices.getAll(LegalForm.class, null));

		this.secondaryLegalFormBeanContainer.removeAllItems();
		this.secondaryLegalFormBeanContainer.addNestedContainerProperty("name.defaultText");
		this.secondaryLegalFormBeanContainer.addAll(this.datasourceServices.getAll(LegalForm.class));
		// add filter to show initial empty combo
		this.secondaryLegalFormBeanContainer.addContainerFilter(new Equal("id", null));

		this.mainSectorContainer.removeAllItems();
		this.mainSectorContainer.addNestedContainerProperty("name.defaultText");
		this.mainSectorContainer.addAll(this.datasourceServices.getAll(Sector.class, null));

		this.secondarySectorContainer.removeAllItems();
		this.secondarySectorContainer.addNestedContainerProperty("name.defaultText");
		this.secondarySectorContainer.addAll(this.datasourceServices.getAll(Sector.class));
		// add filter to show initial empty combo
		this.secondarySectorContainer.addContainerFilter(new Equal("id", null));

		this.entityStateContainer.removeAllItems();
		this.entityStateContainer.addNestedContainerProperty("name.defaultText");
		for (EntityState state : this.datasourceServices.getAll(EntityState.class)) {
			if (!state.getId().equals(EntityState.State.Completed.getId())) {
				this.entityStateContainer.addItem(state);
			}
		}

	}

	@Override
	public List<Layout> getRelatedForms() {
		List<Layout> relatedForms = new ArrayList<Layout>();
		if (this.model.getId() != null) {
			relatedForms.add(this.getRelatedForm(cat.xes.layout.form.related.XesUserRelatedFormLayout.class, this.messageSource.getMessage("entity.user"), this.model, true, Format.OneColumn));
		}
		return relatedForms;
	}

}
