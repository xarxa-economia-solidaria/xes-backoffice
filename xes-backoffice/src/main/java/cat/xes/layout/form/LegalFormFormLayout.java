package cat.xes.layout.form;

import org.jamgo.layout.form.ModelFormLayout;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.exception.NoClassParentRelationDefinedException;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.ui.ComboBox;

import cat.xes.model.LegalForm;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LegalFormFormLayout extends ModelFormLayout<LegalForm> {

	private static final long serialVersionUID = 1L;

	private BeanItemContainer<LegalForm> legalFormsContainer = new BeanItemContainer<LegalForm>(LegalForm.class);

	private JmgLocalizedTextField nameField;

	@Override
	public void update(Item item, LegalForm model) {
		super.update(item, model);

		if (this.model != null) {
			this.nameField.setLocalizedText(this.model.getName());
		}
	}

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws IllegalArgumentException, RepositoryForClassNotDefinedException {
		this.formLayout = formLayout;
		this.binder = binder;

		String nameCaption = this.messageSource.getMessage("location.name");
		this.nameField = new JmgLocalizedTextField(this.model.getName(), this.languages, nameCaption);
		this.nameField.setWidth(50, Unit.PERCENTAGE);
		this.formLayout.addComponent(this.nameField);

		ComboBox parent = this.getComboBox("legalform.parent", "name.defaultText", true, this.legalFormsContainer, 50);
		parent.setWidth(50, Unit.PERCENTAGE);
		formLayout.addComponent(parent);
		this.binder.bind(parent, "parent");

	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException, NoClassParentRelationDefinedException {
		this.legalFormsContainer.removeAllItems();
		this.legalFormsContainer.addAll(this.datasourceServices.getAll(LegalForm.class, null));
	}

	@Override
	public LegalForm save() throws Exception {
		LocalizedString text = this.nameField.getValue();
		this.model.setName(text);
		return super.save();
	}
}
