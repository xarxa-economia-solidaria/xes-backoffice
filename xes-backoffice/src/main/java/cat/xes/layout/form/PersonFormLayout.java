package cat.xes.layout.form;

import org.jamgo.layout.form.ModelFormLayout;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.exception.NoClassParentRelationDefinedException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Property.ValueChangeEvent;
import com.vaadin.v7.data.Property.ValueChangeListener;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.ui.ComboBox;

import cat.xes.model.EntityState;
import cat.xes.model.Person;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Town;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PersonFormLayout extends ModelFormLayout<Person> {

	private static final long serialVersionUID = 1L;

	private ComboBox region;
	private ComboBox province;
	private ComboBox town;

	private BeanItemContainer<Province> provinceBeanContainer = new BeanItemContainer<Province>(Province.class);
	private BeanItemContainer<Region> regionsBeanContainer = new BeanItemContainer<Region>(Region.class);
	private BeanItemContainer<Town> townsBeanContainer = new BeanItemContainer<Town>(Town.class);
	private BeanItemContainer<EntityState> entityStateContainer = new BeanItemContainer<EntityState>(EntityState.class);

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws RepositoryForClassNotDefinedException {

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("entity.active"), "active"));
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.nif"), "nif"));
		binder.getField("nif").setWidth(25, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.name"), "name"));
		binder.getField("name").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.surname"), "surnames"));
		binder.getField("surnames").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.cp"), "cp"));
		binder.getField("cp").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.address"), "address"));
		binder.getField("address").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.email"), "email"));
		binder.getField("email").setWidth(100, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.phone"), "phone"));
		binder.getField("phone").setWidth(100, Unit.PERCENTAGE);

		this.province = this.getComboBox("person.province", "name.defaultText", true, this.provinceBeanContainer, 50);
		formLayout.addComponent(this.province);

		this.region = this.getComboBox("person.region", "name.defaultText", true, this.regionsBeanContainer, 50);
		formLayout.addComponent(this.region);

		this.town = this.getComboBox("person.town", "name.defaultText", true, this.townsBeanContainer, 50);
		formLayout.addComponent(this.town);
		this.binder.bind(this.town, "town");
		this.region.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Region region = (Region) event.getProperty().getValue();
				PersonFormLayout.this.townsBeanContainer.removeAllItems();
				if (region != null) {
					PersonFormLayout.this.townsBeanContainer.addAll(region.getTowns());
				}
				PersonFormLayout.this.town.setValue(null);
			}
		});
		this.province.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Province province = (Province) event.getProperty().getValue();
				PersonFormLayout.this.regionsBeanContainer.removeAllItems();
				PersonFormLayout.this.townsBeanContainer.removeAllItems();
				if (province != null) {
					PersonFormLayout.this.regionsBeanContainer.addAll(province.getRegions());
				}
				PersonFormLayout.this.region.setValue(null);
				PersonFormLayout.this.town.setValue(null);
			}
		});

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.othertown"), "otherTown"));
		binder.getField("otherTown").setWidth(100, Unit.PERCENTAGE);

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.account.owner"), "bankAccountOwner"));
		binder.getField("bankAccountOwner").setWidth(50, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.account.iban"), "bankAccountIban"));
		binder.getField("bankAccountIban").setWidth(50, Unit.PERCENTAGE);

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.quote"), "quote"));
		binder.getField("quote").setWidth(50, Unit.PERCENTAGE);
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.quote.voluntary"), "voluntaryQuote"));
		binder.getField("voluntaryQuote").setWidth(50, Unit.PERCENTAGE);

		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.date.request"), "registryRequestDate"));
		formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("person.date.resolution"), "registryResolutionDate"));

		ComboBox states = this.getComboBox("person.state", "name.defaultText", true, this.entityStateContainer, 25);
		formLayout.addComponent(states);
		this.binder.bind(states, "state");
	}

	@Override
	public void update(Item item, Person model) {
		super.update(item, model);
		if (model != null) {
			Town town = model.getTown();
			if (town != null) {
				Region region = town.getRegion();
				if (region != null) {
					Province province = region.getProvince();
					this.province.setValue(province);
					this.region.setValue(region);
					this.town.setValue(town);
				}
			}
		}
	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException, NoClassParentRelationDefinedException {
		this.provinceBeanContainer.removeAllItems();
		this.provinceBeanContainer.addNestedContainerProperty("name.defaultText");
		this.provinceBeanContainer.addAll(this.datasourceServices.getAll(Province.class));

		this.entityStateContainer.removeAllItems();
		this.entityStateContainer.addNestedContainerProperty("name.defaultText");
		for (EntityState state : this.datasourceServices.getAll(EntityState.class)) {
			if (!state.getId().equals(EntityState.State.Completed.getId())) {
				this.entityStateContainer.addItem(state);
			}
		}

	}

}
