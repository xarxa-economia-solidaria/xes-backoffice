package cat.xes.layout.form;

import java.util.List;

import org.jamgo.layout.form.ModelFormLayout;
import org.jamgo.model.jpa.entity.Model;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.fieldgroup.FieldGroup;

import cat.xes.model.Province;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProvinceFormLayout extends ModelFormLayout<Province> {

	private static final long serialVersionUID = 1L;

	private JmgLocalizedTextField nameField;

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws IllegalArgumentException, RepositoryForClassNotDefinedException {
		String nameCaption = this.messageSource.getMessage("location.name");
		this.nameField = new JmgLocalizedTextField(this.model != null ? this.model.getName() : null, this.languages, nameCaption);
		this.nameField.setWidth(100, Unit.PERCENTAGE);
		this.formLayout.addComponent(this.nameField);
	}

	@Override
	public void update(Item item, Province model) {
		super.update(item, model);

		if (this.model != null) {
			this.nameField.setLocalizedText(this.model.getName());
		}

	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException {
	}

	@Override
	public Province save() throws Exception {
		this.model.setName(this.nameField.getValue());
		return super.save();
	}

}
