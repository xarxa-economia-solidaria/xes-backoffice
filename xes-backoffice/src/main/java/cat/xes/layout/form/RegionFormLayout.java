package cat.xes.layout.form;

import org.jamgo.layout.form.ModelFormLayout;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.ui.ComboBox;

import cat.xes.model.Province;
import cat.xes.model.Region;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RegionFormLayout extends ModelFormLayout<Region> {

	private static final long serialVersionUID = 1L;

	private BeanItemContainer<Province> provinceContainer = new BeanItemContainer<Province>(Province.class);

	private JmgLocalizedTextField nameField;

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws IllegalArgumentException, RepositoryForClassNotDefinedException {
		String nameCaption = this.messageSource.getMessage("location.name");
		this.nameField = new JmgLocalizedTextField(this.model.getName(), this.languages, nameCaption);
		this.nameField.setWidth(50, Unit.PERCENTAGE);
		this.formLayout.addComponent(this.nameField);

		ComboBox province = this.getComboBox("location.province", "name.defaultText", true, this.provinceContainer, 25);
		province.setWidth(50, Unit.PERCENTAGE);
		formLayout.addComponent(province);
		this.binder.bind(province, "province");
	}

	@Override
	public void update(Item item, Region model) {
		super.update(item, model);

		if (this.model != null) {
			this.nameField.setLocalizedText(this.model.getName());
		}
	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException {
		this.provinceContainer.removeAllItems();
		this.provinceContainer.addAll(this.datasourceServices.getAll(Province.class));
	}

	@Override
	public Region save() throws Exception {
		LocalizedString text = this.nameField.getValue();
		this.model.setName(text);
		return super.save();
	}

}
