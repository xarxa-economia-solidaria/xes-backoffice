package cat.xes.layout.form;

import org.jamgo.layout.form.ModelFormLayout;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.vaadin.ui.JmgLocalizedTextField;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.ui.ComboBox;

import cat.xes.model.Region;
import cat.xes.model.Town;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TownFormLayout extends ModelFormLayout<Town> {

	private static final long serialVersionUID = 1L;

	private JmgLocalizedTextField nameField;

	private BeanItemContainer<Region> regionsContainer = new BeanItemContainer<Region>(Region.class);

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws IllegalArgumentException, RepositoryForClassNotDefinedException {
		String nameCaption = this.messageSource.getMessage("location.name");
		this.nameField = new JmgLocalizedTextField(this.model.getName(), this.languages, nameCaption);
		this.nameField.setWidth(50, Unit.PERCENTAGE);
		this.formLayout.addComponent(this.nameField);

		ComboBox region = this.getComboBox("location.region", "name.defaultText", true, this.regionsContainer, 25);
		this.formLayout.addComponent(region);
		this.binder.bind(region, "region");
	}

	@Override
	public void update(Item item, Town model) {
		super.update(item, model);

		if (this.model != null) {
			this.nameField.setLocalizedText(this.model.getName());
		}
	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException {
		this.regionsContainer.removeAllItems();
		this.regionsContainer.addAll(this.datasourceServices.getAll(Region.class));
	}

	@Override
	public Town save() throws Exception {
		this.model.setName(this.nameField.getValue());
		return super.save();
	}

}
