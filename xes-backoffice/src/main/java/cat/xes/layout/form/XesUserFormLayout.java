package cat.xes.layout.form;

import org.jamgo.layout.form.UserFormLayout;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.v7.data.fieldgroup.FieldGroup;

import cat.xes.model.XesUser;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class XesUserFormLayout extends UserFormLayout<XesUser> {

	private static final long serialVersionUID = 1L;

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws RepositoryForClassNotDefinedException {
		super.initializeForm(formLayout, binder);

		this.formLayout.addComponent(binder.buildAndBind(this.messageSource.getMessage("user.phone"), "phone"));
		this.binder.getField("phone").setWidth(50, Unit.PERCENTAGE);
	}

}
