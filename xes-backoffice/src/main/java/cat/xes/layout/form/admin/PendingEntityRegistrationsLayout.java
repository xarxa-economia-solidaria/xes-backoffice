package cat.xes.layout.form.admin;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.jamgo.ResourceBundleMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.util.BeanItem;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.event.ItemClickEvent;
import com.vaadin.v7.event.ItemClickEvent.ItemClickListener;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Table;
import com.vaadin.v7.ui.Table.ColumnGenerator;
import com.vaadin.v7.ui.VerticalLayout;

import cat.xes.layout.form.EntityFormLayout;
import cat.xes.model.Entity;
import cat.xes.service.RegisterServces;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PendingEntityRegistrationsLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	private RegisterServces registerService;
	@Autowired
	private ResourceBundleMessageSource messageSource;
	@Autowired
	private EntityFormLayout entityFormLayout;

	private BeanItemContainer<Entity> pendingEntitiesBeanContainer;

	private Table table;

	@PostConstruct
	public void init() {
		this.setSizeFull();

		this.pendingEntitiesBeanContainer = new BeanItemContainer<Entity>(Entity.class);
		this.pendingEntitiesBeanContainer.addNestedContainerProperty("user.name");

		this.table = new Table() {
			private static final long serialVersionUID = 1L;

			@Override
			protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
				Object date = property.getValue();
				if (date instanceof Date) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					return sdf.format(date).toString();
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		this.table.setSizeFull();
		this.table.setSelectable(true);
		this.table.addGeneratedColumn("action", new ColumnGenerator() {
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, final Object itemId, Object columnId) {
				Button accept = new Button(PendingEntityRegistrationsLayout.this.messageSource.getMessage("action.accept"));
				accept.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							PendingEntityRegistrationsLayout.this.registerService.acceptEntityRegistrationRequest(((Entity) itemId).getId());
							PendingEntityRegistrationsLayout.this.table.removeItem(itemId);
						} catch (Exception e) {
							e.printStackTrace();
							Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
						}
					}
				});
				Button decline = new Button(PendingEntityRegistrationsLayout.this.messageSource.getMessage("action.decline"));
				decline.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							PendingEntityRegistrationsLayout.this.registerService.declineEntityRegistrationRequest(((Entity) itemId).getId());
							PendingEntityRegistrationsLayout.this.table.removeItem(itemId);
						} catch (Exception e) {
							e.printStackTrace();
							Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
						}
					}
				});
				HorizontalLayout actionsLayout = new HorizontalLayout(accept, decline);
				actionsLayout.setSpacing(true);
				return actionsLayout;
			}
		});
		this.table.setContainerDataSource(this.pendingEntitiesBeanContainer);
		this.table.setVisibleColumns("nif", "name", "user.name", "email", "xesRegistryRequestDate", "xesRegistryResolutionDate", "action");
		this.table.setColumnHeaders(
			this.messageSource.getMessage("register.nif"),
			this.messageSource.getMessage("register.name"),
			this.messageSource.getMessage("register.contact.name"),
			this.messageSource.getMessage("register.email"),
			this.messageSource.getMessage("register.requestdate"),
			this.messageSource.getMessage("register.registrydate"),
			"");
		this.table.setPageLength(15);
		this.table.addItemClickListener(new ItemClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void itemClick(ItemClickEvent event) {
				BeanItem<Entity> item = PendingEntityRegistrationsLayout.this.pendingEntitiesBeanContainer.getItem(event.getItemId());
				PendingEntityRegistrationsLayout.this.entityFormLayout.init(item, item.getBean(), false);
				PendingEntityRegistrationsLayout.this.addComponent(PendingEntityRegistrationsLayout.this.entityFormLayout);
			}
		});
		this.addComponent(this.table);
	}

	@Override
	public void attach() {
		super.attach();
		this.pendingEntitiesBeanContainer.addAll(this.registerService.getPendingRegistrationEntities());
	}

}
