package cat.xes.layout.form.admin;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.jamgo.ResourceBundleMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.util.BeanItem;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.event.ItemClickEvent;
import com.vaadin.v7.event.ItemClickEvent.ItemClickListener;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Table;
import com.vaadin.v7.ui.Table.ColumnGenerator;
import com.vaadin.v7.ui.VerticalLayout;

import cat.xes.layout.form.PersonFormLayout;
import cat.xes.model.Person;
import cat.xes.service.RegisterServces;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PendingPersonRegistrationsLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	private RegisterServces registerService;
	@Autowired
	private ResourceBundleMessageSource messageSource;
	@Autowired
	private PersonFormLayout entityFormLayout;

	private BeanItemContainer<Person> pendingPersonsBeanContainer;

	private Table table;

	@PostConstruct
	public void init() {
		this.setSizeFull();

		this.pendingPersonsBeanContainer = new BeanItemContainer<Person>(Person.class);

		this.table = new Table() {
			private static final long serialVersionUID = 1L;

			@Override
			protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
				Object date = property.getValue();
				if (date instanceof Date) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					return sdf.format(date).toString();
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		this.table.setSizeFull();
		this.table.setSelectable(true);
		this.table.addGeneratedColumn("action", new ColumnGenerator() {
			private static final long serialVersionUID = 1L;

			@Override
			public Object generateCell(Table source, final Object itemId, Object columnId) {
				Button accept = new Button(PendingPersonRegistrationsLayout.this.messageSource.getMessage("action.accept"));
				accept.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							PendingPersonRegistrationsLayout.this.registerService.acceptPersonRegistrationRequest(((Person) itemId).getId());
							PendingPersonRegistrationsLayout.this.table.removeItem(itemId);
						} catch (Exception e) {
							e.printStackTrace();
							Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
						}
					}
				});
				Button decline = new Button(PendingPersonRegistrationsLayout.this.messageSource.getMessage("action.decline"));
				decline.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							PendingPersonRegistrationsLayout.this.registerService.declinePersonRegistrationRequest(((Person) itemId).getId());
							PendingPersonRegistrationsLayout.this.table.removeItem(itemId);
						} catch (Exception e) {
							e.printStackTrace();
							Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
						}
					}
				});
				HorizontalLayout actionsLayout = new HorizontalLayout(accept, decline);
				actionsLayout.setSpacing(true);
				return actionsLayout;
			}
		});
		this.table.setContainerDataSource(this.pendingPersonsBeanContainer);
		this.table.setVisibleColumns("nif", "name", "email", "registryRequestDate", "registryResolutionDate", "action");
		this.table.setColumnHeaders(
			this.messageSource.getMessage("register.nif"),
			this.messageSource.getMessage("register.name"),
			this.messageSource.getMessage("register.email"),
			this.messageSource.getMessage("register.requestdate"),
			this.messageSource.getMessage("register.registrydate"),
			"");
		this.table.setPageLength(15);
		this.table.addItemClickListener(new ItemClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void itemClick(ItemClickEvent event) {
				BeanItem<Person> item = PendingPersonRegistrationsLayout.this.pendingPersonsBeanContainer.getItem(event.getItemId());
				PendingPersonRegistrationsLayout.this.entityFormLayout.init(item, item.getBean(), false);
				PendingPersonRegistrationsLayout.this.addComponent(PendingPersonRegistrationsLayout.this.entityFormLayout);
			}
		});
		this.addComponent(this.table);
	}

	@Override
	public void attach() {
		super.attach();
		this.pendingPersonsBeanContainer.addAll(this.registerService.getPendingRegistrationPersons());
	}

}
