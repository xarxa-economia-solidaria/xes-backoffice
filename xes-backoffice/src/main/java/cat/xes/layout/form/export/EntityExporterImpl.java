package cat.xes.layout.form.export;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import org.jamgo.ResourceBundleMessageSource;
import org.jamgo.model.jpa.portable.ImportStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cat.xes.model.Entity;
import cat.xes.model.export.EntityExporter;

@Component
public class EntityExporterImpl implements EntityExporter {

	@Autowired
	private ResourceBundleMessageSource messageSource;

	@Override
	public ImportStats<Entity> extractData(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportStats<Entity> refreshImport() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getColumnNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getColumnHeaders() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getAllColumns() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getErrors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setVisibleColumns(boolean[] bitMap) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean[] getVisibleColumns() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportStats<Entity> doImport() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDeleteAllBeforeImport(Boolean value) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean deleteAllBeforeImport() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public InputStream export(List<Entity> entities) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder entitiesSB = new StringBuilder();
		entitiesSB.append(this.messageSource.getMessage("entity.nif")).append(",")
			.append(this.messageSource.getMessage("entity.name")).append(",")
			.append(this.messageSource.getMessage("entity.shortname")).append(",")
			.append(this.messageSource.getMessage("entity.description")).append(",")
			.append(this.messageSource.getMessage("entity.address")).append(",")
			.append(this.messageSource.getMessage("entity.cp")).append(",")
			.append(this.messageSource.getMessage("entity.email")).append(",")
			.append(this.messageSource.getMessage("entity.phone")).append(",")
			.append(this.messageSource.getMessage("entity.web")).append(",")
			.append(this.messageSource.getMessage("entity.year")).append(",")
			.append(this.messageSource.getMessage("entity.pamapam")).append(",")
			.append(this.messageSource.getMessage("entity.legalform.main")).append(",")
			.append(this.messageSource.getMessage("entity.legalform.secondary")).append(",")
			.append(this.messageSource.getMessage("entity.sector.main")).append(",")
			.append(this.messageSource.getMessage("entity.sector.secondary")).append(",")
			.append(this.messageSource.getMessage("entity.province")).append(",")
			.append(this.messageSource.getMessage("entity.region")).append(",")
			.append(this.messageSource.getMessage("entity.town")).append(",")
			.append(this.messageSource.getMessage("entity.othertown")).append(",")
			.append(this.messageSource.getMessage("entity.administration.contact")).append(",")
			.append(this.messageSource.getMessage("entity.account.owner")).append(",")
			.append(this.messageSource.getMessage("entity.account.iban")).append(",")
			.append(this.messageSource.getMessage("entity.date.request")).append(",")
			.append(this.messageSource.getMessage("entity.date.resolution")).append(",")
			.append("\n");
		for (Entity entity : entities) {
			String nif = entity.getNif() != null ? entity.getNif() : "";
			String name = entity.getName() != null ? entity.getName() : "";
			String address = entity.getAddress() != null ? entity.getAddress() : "";
			String administrationContact = entity.getAdministrationContact() != null ? entity.getAdministrationContact() : "";
			String bankAccountIban = entity.getBankAccountIban() != null ? entity.getBankAccountIban() : "";
			String bankAccountOwner = entity.getBankAccountOwner() != null ? entity.getBankAccountOwner() : "";
			String cp = entity.getCp() != null ? entity.getCp() : "";
			String description = entity.getDescription() != null ? entity.getDescription() : "";
			String email = entity.getEmail() != null ? entity.getEmail() : "";
			String otherTown = entity.getOtherTown() != null ? entity.getOtherTown() : "";
			String pamAPamUrl = entity.getPamAPamUrl() != null ? entity.getPamAPamUrl() : "";
			String phone = entity.getPhone() != null ? entity.getPhone() : "";
			String shortName = entity.getShortName() != null ? entity.getShortName() : "";
			String web = entity.getWeb() != null ? entity.getWeb() : "";
			String year = entity.getYear() != null ? entity.getYear() : "";
			String legalForm = entity.getLegalForm() != null ? entity.getLegalForm().getName().getDefaultText() : "";
			String legalFormParent = "";
			if (entity.getLegalForm() != null) {
				legalFormParent = entity.getLegalForm().getParent() != null ? entity.getLegalForm().getParent().getName().getDefaultText() : "";
			}
			String sector = entity.getSector() != null ? entity.getSector().getName().getDefaultText() : "";
			String sectorParent = "";
			if (entity.getSector() != null) {
				sectorParent = entity.getSector().getParent() != null ? entity.getSector().getName().getDefaultText() : "";
			}
			String province = entity.getProvince() != null ? entity.getProvince().getName().getDefaultText() : "";
			String region = entity.getRegion() != null ? entity.getRegion().getName().getDefaultText() : "";
			String town = entity.getTown() != null ? entity.getTown().getName().getDefaultText() : "";
			String registryRequestDate = entity.getXesRegistryRequestDate() != null ? sdf.format(entity.getXesRegistryRequestDate()) : "";
			String registryResolutionDate = entity.getXesRegistryResolutionDate() != null ? sdf.format(entity.getXesRegistryResolutionDate()) : "";
			entitiesSB.append(escapeForCsv(nif)).append(",")
				.append(escapeForCsv(name)).append(",")
				.append(escapeForCsv(shortName)).append(",")
				.append(escapeForCsv(description)).append(",")
				.append(escapeForCsv(address)).append(",")
				.append(escapeForCsv(cp)).append(",")
				.append(escapeForCsv(email)).append(",")
				.append(escapeForCsv(phone)).append(",")
				.append(escapeForCsv(web)).append(",")
				.append(escapeForCsv(year)).append(",")
				.append(escapeForCsv(pamAPamUrl)).append(",")
				.append(escapeForCsv(legalFormParent)).append(",")
				.append(escapeForCsv(legalForm)).append(",")
				.append(escapeForCsv(sectorParent)).append(",")
				.append(escapeForCsv(sector)).append(",")
				.append(escapeForCsv(province)).append(",")
				.append(escapeForCsv(region)).append(",")
				.append(escapeForCsv(town)).append(",")
				.append(escapeForCsv(otherTown)).append(",")
				.append(escapeForCsv(administrationContact)).append(",")
				.append(escapeForCsv(bankAccountOwner)).append(",")
				.append(escapeForCsv(bankAccountIban)).append(",")
				.append(escapeForCsv(registryRequestDate)).append(",")
				.append(escapeForCsv(registryResolutionDate))
				.append("\n");
		}
		return new ByteArrayInputStream(entitiesSB.toString().getBytes());
	}

	private static String escapeForCsv(String text) {
		return "\"".concat(text).concat("\"");
	}
}
