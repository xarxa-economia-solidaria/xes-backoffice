package cat.xes.layout.form.export;

import org.jamgo.layout.ExportLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cat.xes.model.Person;
import cat.xes.model.Person;

@Component()
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PersonExportLayout extends ExportLayout<Person> {

	private static final long serialVersionUID = 1L;

}
