package cat.xes.layout.form.export;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import org.jamgo.ResourceBundleMessageSource;
import org.jamgo.model.jpa.portable.ImportStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cat.xes.model.Person;
import cat.xes.model.export.PersonExporter;

@Component
public class PersonExporterImpl implements PersonExporter {

	@Autowired
	private ResourceBundleMessageSource messageSource;

	@Override
	public ImportStats<Person> extractData(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportStats<Person> refreshImport() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getColumnNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getColumnHeaders() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getAllColumns() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getErrors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setVisibleColumns(boolean[] bitMap) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean[] getVisibleColumns() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImportStats<Person> doImport() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDeleteAllBeforeImport(Boolean value) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean deleteAllBeforeImport() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public InputStream export(List<Person> persons) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder entitiesSB = new StringBuilder();
		entitiesSB.append(this.messageSource.getMessage("entity.nif")).append(",")
			.append(this.messageSource.getMessage("person.name")).append(",")
			.append(this.messageSource.getMessage("person.address")).append(",")
			.append(this.messageSource.getMessage("person.cp")).append(",")
			.append(this.messageSource.getMessage("person.email")).append(",")
			.append(this.messageSource.getMessage("person.phone")).append(",")
			.append(this.messageSource.getMessage("person.province")).append(",")
			.append(this.messageSource.getMessage("person.region")).append(",")
			.append(this.messageSource.getMessage("person.town")).append(",")
			.append(this.messageSource.getMessage("person.othertown")).append(",")
			.append(this.messageSource.getMessage("person.account.owner")).append(",")
			.append(this.messageSource.getMessage("person.account.iban")).append(",")
			.append(this.messageSource.getMessage("person.quote")).append(",")
			.append(this.messageSource.getMessage("person.quote.voluntary")).append(",")
			.append(this.messageSource.getMessage("person.date.request")).append(",")
			.append(this.messageSource.getMessage("person.date.resolution")).append(",")
			.append("\n");
		for (Person person : persons) {
			String nif = person.getNif() != null ? person.getNif() : "";
			String name = person.getName() != null ? person.getName() : "";
			String address = person.getAddress() != null ? person.getAddress() : "";
			String email = person.getEmail() != null ? person.getEmail() : "";
			String cp = person.getCp() != null ? person.getCp() : "";
			String otherTown = person.getOtherTown() != null ? person.getOtherTown() : "";
			String province = person.getProvince() != null ? person.getProvince().getName().getDefaultText() : "";
			String region = person.getRegion() != null ? person.getRegion().getName().getDefaultText() : "";
			String town = person.getTown() != null ? person.getTown().getName().getDefaultText() : "";
			String phone = person.getPhone() != null ? person.getPhone() : "";
			String bankAccountOwner = person.getBankAccountOwner() != null ? person.getBankAccountOwner() : "";
			String bankAccountIban = person.getBankAccountIban() != null ? person.getBankAccountIban() : "";
			String quote = person.getQuote() != null ? person.getQuote() : "";
			String voluntaryQuote = person.getVoluntaryQuote() != null ? person.getVoluntaryQuote() : "";
			String registryRequestDate = person.getRegistryRequestDate() != null ? sdf.format(person.getRegistryRequestDate()) : "";
			String registryResolutionDate = person.getRegistryResolutionDate() != null ? sdf.format(person.getRegistryResolutionDate()) : "";
			entitiesSB.append(escapeForCsv(nif)).append(",")
				.append(escapeForCsv(name)).append(",")
				.append(escapeForCsv(address)).append(",")
				.append(escapeForCsv(cp)).append(",")
				.append(escapeForCsv(email)).append(",")
				.append(escapeForCsv(phone)).append(",")
				.append(escapeForCsv(province)).append(",")
				.append(escapeForCsv(region)).append(",")
				.append(escapeForCsv(town)).append(",")
				.append(escapeForCsv(otherTown)).append(",")
				.append(escapeForCsv(bankAccountOwner)).append(",")
				.append(escapeForCsv(bankAccountIban)).append(",")
				.append(escapeForCsv(quote)).append(",")
				.append(escapeForCsv(voluntaryQuote)).append(",")
				.append(escapeForCsv(registryRequestDate)).append(",")
				.append(escapeForCsv(registryResolutionDate))
				.append("\n");
		}
		return new ByteArrayInputStream(entitiesSB.toString().getBytes());
	}

	private static String escapeForCsv(String text) {
		return "\"".concat(text).concat("\"");
	}
}
