package cat.xes.layout.form.related;

import java.util.Collection;
import java.util.List;

import org.jamgo.converter.ListToSetConverter;
import org.jamgo.layout.form.PasswordFormLayout;
import org.jamgo.layout.form.RelatedModelFormLayout;
import org.jamgo.model.jpa.entity.Language;
import org.jamgo.model.jpa.entity.Model;
import org.jamgo.model.jpa.entity.Role;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Layout;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.v7.data.util.BeanItem;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.ui.ComboBox;
import com.vaadin.v7.ui.OptionGroup;
import com.vaadin.v7.ui.PasswordField;

import cat.xes.model.XesUser;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class XesUserRelatedFormLayout extends RelatedModelFormLayout<cat.xes.model.Entity> {

	private static final long serialVersionUID = 1L;

	private BeanItemContainer<Role> roleContainer = new BeanItemContainer<Role>(Role.class);
	private BeanItemContainer<Language> languageContainer = new BeanItemContainer<Language>(Language.class);

	@Autowired
	private PasswordFormLayout passwordFormLayout;

	private Button editPassword;

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws RepositoryForClassNotDefinedException {
		this.formLayout = formLayout;
//		this.binder = binder;
		this.binder = new FieldGroup(new BeanItem<XesUser>(this.model.getUser()));
//		this.binder.setItemDataSource(new BeanItem<XesUser>(this.model.getUser()));

		this.formLayout.addComponent(this.binder.buildAndBind(this.messageSource.getMessage("user.name"), "name"));
		this.binder.getField("name").setWidth(50, Unit.PERCENTAGE);

		this.formLayout.addComponent(this.binder.buildAndBind(this.messageSource.getMessage("user.surname"), "surname"));
		this.binder.getField("surname").setWidth(50, Unit.PERCENTAGE);

		this.formLayout.addComponent(this.binder.buildAndBind(this.messageSource.getMessage("user.email"), "email"));
		this.binder.getField("email").setWidth(50, Unit.PERCENTAGE);

		this.formLayout.addComponent(this.binder.buildAndBind(this.messageSource.getMessage("user.phone"), "phone"));
		this.binder.getField("phone").setWidth(50, Unit.PERCENTAGE);

		ComboBox languageCombo = this.getComboBox("user.language", "name", false, this.languageContainer, 25);
		formLayout.addComponent(languageCombo);
		this.binder.bind(languageCombo, "language");

		this.formLayout.addComponent(this.binder.buildAndBind(this.messageSource.getMessage("user.username"), "username"));
		this.binder.getField("username").setWidth(50, Unit.PERCENTAGE);
		PasswordField passwordField = this.binder.buildAndBind(this.messageSource.getMessage("user.password"), "password", PasswordField.class);
		passwordField.setRequired(true);

		this.editPassword = new Button();
		this.editPassword.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				Window window = new Window(XesUserRelatedFormLayout.this.messageSource.getMessage("user.password.edit"));
				window.setModal(true);
				window.setWidth(30, Unit.PERCENTAGE);
				window.center();

				XesUserRelatedFormLayout.this.passwordFormLayout.setPasswordField(passwordField);
				window.setContent(XesUserRelatedFormLayout.this.passwordFormLayout);
				UI.getCurrent().addWindow(window);
			}
		});
		this.formLayout.addComponent(this.editPassword);

		this.formLayout.addComponent(this.binder.buildAndBind(this.messageSource.getMessage("user.active"), "enabled"));

		OptionGroup rolesOptionGroup = new OptionGroup(this.messageSource.getMessage("user.roles"), this.roleContainer);
		rolesOptionGroup.setMultiSelect(true);
		rolesOptionGroup.setConverter(new ListToSetConverter());
		this.formLayout.addComponent(rolesOptionGroup);
		this.binder.bind(rolesOptionGroup, "roles");

	}

	@Override
	public void update(Item item, cat.xes.model.Entity model) {
		super.update(new BeanItem(model.getUser()), model);

		String passwordButtonCaption = null;
		if (this.binder.getField("password").isEmpty()) {
			passwordButtonCaption = this.messageSource.getMessage("user.password.set");
		} else {
			passwordButtonCaption = this.messageSource.getMessage("user.password.edit");
		}
		this.editPassword.setCaption(passwordButtonCaption);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException {
		this.roleContainer.removeAllItems();
		this.roleContainer.addAll((Collection<? extends Role>) this.repositoryFactory.getRepository(Role.class).findAll());

		this.languageContainer.removeAllItems();
		this.languageContainer.addAll((Collection<Language>) this.repositoryFactory.getRepository(Language.class).findAll());

	}

	@Override
	public List<Layout> getRelatedForms() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Model updateParentEntity(Model parentModel) {
		super.updateParentEntity(parentModel);
		try {
			this.binder.commit();
		} catch (CommitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		cat.xes.model.Entity entity = (cat.xes.model.Entity) parentModel;
		XesUser user = this.model.getUser();
		user.setEntity(entity);
		return entity;
	}

}