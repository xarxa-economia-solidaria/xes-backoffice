package cat.xes.layout.form.search;

import java.util.List;

import org.jamgo.layout.form.search.SearchModelFormLayout;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.exception.NoClassParentRelationDefinedException;
import org.jamgo.vaadin.ui.JmgTwoColumnFormLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.v7.data.Container.Filter;
import com.vaadin.v7.data.Property.ValueChangeEvent;
import com.vaadin.v7.data.Property.ValueChangeListener;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.data.util.filter.Compare;
import com.vaadin.v7.ui.CheckBox;
import com.vaadin.v7.ui.ComboBox;
import com.vaadin.v7.ui.DateField;
import com.vaadin.v7.ui.TextField;

import cat.xes.model.Entity;
import cat.xes.model.LegalForm;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EntitySearchFormLayout extends SearchModelFormLayout<Entity> {

	private static final long serialVersionUID = 1L;

	private ComboBox mainSector;
	private ComboBox secondarySector;

	private ComboBox secondaryLegalForm;
	private ComboBox mainLegalForm;

	private ComboBox region;
	private ComboBox province;
	private ComboBox town;

	private DateField fromRegistryResolutionDate;
	private DateField toRegistryResolutionDate;

	private BeanItemContainer<Province> provinceBeanContainer = new BeanItemContainer<Province>(Province.class);
	private BeanItemContainer<Region> regionsBeanContainer = new BeanItemContainer<Region>(Region.class);
	private BeanItemContainer<Town> townsBeanContainer = new BeanItemContainer<Town>(Town.class);
	private BeanItemContainer<LegalForm> mainLegalFormBeanContainer = new BeanItemContainer<LegalForm>(LegalForm.class);
	private BeanItemContainer<Sector> sectorContainer = new BeanItemContainer<Sector>(Sector.class);
	private BeanItemContainer<Sector> mainSectorContainer = new BeanItemContainer<Sector>(Sector.class);

	private Filter mainSectorFilter;
	private Filter mainLegalFormFilter;
	private Filter regionFilter;
	private Filter provinceFilter;
	private Filter fromRegistryAcceptanceDateFilter;
	private Filter toRegistryAcceptanceDateFilter;

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws IllegalArgumentException, RepositoryForClassNotDefinedException {

		JmgTwoColumnFormLayout layout = (JmgTwoColumnFormLayout) formLayout;

		layout.addComponentToFirstColumn(binder.buildAndBind(this.messageSource.getMessage("entity.nif"), "nif"));
		((TextField) binder.getField("nif")).setNullRepresentation("");
		layout.addComponentToFirstColumn(binder.buildAndBind(this.messageSource.getMessage("entity.name"), "name"));
		((TextField) binder.getField("name")).setNullRepresentation("");

		this.mainLegalForm = this.getComboBox("entity.legalform.main", "name.defaultText", true, this.mainLegalFormBeanContainer, 50);
		layout.addComponentToFirstColumn(this.mainLegalForm);

		final BeanItemContainer<LegalForm> legalFormContainer = new BeanItemContainer<LegalForm>(LegalForm.class);
		this.secondaryLegalForm = this.getComboBox("entity.legalform.secondary", "name.defaultText", true, legalFormContainer, 50);
		layout.addComponentToFirstColumn(this.secondaryLegalForm);
		this.binder.bind(this.secondaryLegalForm, "legalForm");
		this.mainLegalForm.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					legalFormContainer.removeAllItems();
					if (EntitySearchFormLayout.this.mainLegalForm.getValue() != null) {
						legalFormContainer.addAll(EntitySearchFormLayout.this.datasourceServices.getAll(LegalForm.class, EntitySearchFormLayout.this.mainLegalForm.getValue()));
					}
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});

		this.mainSector = this.getComboBox("entity.sector.main", "name.defaultText", true, this.mainSectorContainer, 50);
		layout.addComponentToFirstColumn(this.mainSector);

		this.secondarySector = this.getComboBox("entity.sector.secondary", "name.defaultText", true, this.sectorContainer, 50);
		layout.addComponentToFirstColumn(this.secondarySector);
		this.binder.bind(this.secondarySector, "sector");
		this.mainSector.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					EntitySearchFormLayout.this.sectorContainer.removeAllItems();
					if (EntitySearchFormLayout.this.mainSector.getValue() != null) {
						EntitySearchFormLayout.this.sectorContainer.addAll(EntitySearchFormLayout.this.datasourceServices.getAll(Sector.class, EntitySearchFormLayout.this.mainSector.getValue()));
					}
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});

		layout.addComponentToFirstColumn(binder.buildAndBind(this.messageSource.getMessage("entity.active"), "active"));
		((CheckBox) binder.getField("active")).setValue(true);

		layout.addComponentToFirstColumn(binder.buildAndBind(this.messageSource.getMessage("entity.collaborator"), "collaborator"));

		this.province = this.getComboBox("entity.province", "name.defaultText", true, this.provinceBeanContainer, 50);
		layout.addComponentToSecondColumn(this.province);

		this.region = this.getComboBox("entity.region", "name.defaultText", true, this.regionsBeanContainer, 50);
		layout.addComponentToSecondColumn(this.region);

		this.town = this.getComboBox("entity.town", "name.defaultText", true, this.townsBeanContainer, 50);
		layout.addComponentToSecondColumn(this.town);
		this.binder.bind(this.town, "town");
		this.region.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					Region region = (Region) event.getProperty().getValue();
					EntitySearchFormLayout.this.townsBeanContainer.removeAllItems();
					if (region != null) {
						List<Town> towns = EntitySearchFormLayout.this.datasourceServices.getAll(Town.class, region);
						EntitySearchFormLayout.this.townsBeanContainer.addAll(towns);
					}
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});
		this.province.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					Province province = (Province) event.getProperty().getValue();
					EntitySearchFormLayout.this.regionsBeanContainer.removeAllItems();
					EntitySearchFormLayout.this.townsBeanContainer.removeAllItems();
					if (province != null) {
						List<Region> regions = EntitySearchFormLayout.this.datasourceServices.getAll(Region.class, province);
						EntitySearchFormLayout.this.regionsBeanContainer.addAll(regions);
					}
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});

		layout.addComponentToSecondColumn(binder.buildAndBind(this.messageSource.getMessage("entity.othertown"), "otherTown"));
		((TextField) binder.getField("otherTown")).setNullRepresentation("");
		binder.getField("otherTown").setWidth(100, Unit.PERCENTAGE);

		this.fromRegistryResolutionDate = new DateField("Data acceptació (des de)");
		this.fromRegistryResolutionDate.setDateFormat("dd/MM/yyyy");
		this.toRegistryResolutionDate = new DateField("Data acceptació (fins)");
		this.toRegistryResolutionDate.setDateFormat("dd/MM/yyyy");

		layout.addComponentToSecondColumn(this.fromRegistryResolutionDate);
		layout.addComponentToSecondColumn(this.toRegistryResolutionDate);

	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException, NoClassParentRelationDefinedException {
		this.provinceBeanContainer.removeAllItems();
		this.provinceBeanContainer.addAll(this.datasourceServices.getAll(Province.class));

		this.mainLegalFormBeanContainer.removeAllItems();
		this.mainLegalFormBeanContainer.addAll(this.datasourceServices.getAll(LegalForm.class, null));

		this.mainSectorContainer.removeAllItems();
		this.mainSectorContainer.addAll(this.datasourceServices.getAll(Sector.class, null));
	}

	@Override
	protected void search() {
		super.search();

		if (this.region.getValue() != null) {
			this.regionFilter = new Compare.Equal("town.region", this.region.getValue());
			this.table.addFilter("town.region", this.regionFilter);
		} else {
			this.table.removeFilter("town.region");
			this.table.removeFilter("town");

		}
		if (this.province.getValue() != null) {
			this.provinceFilter = new Compare.Equal("town.region.province", this.province.getValue());
			this.table.addFilter("town.region.province", this.provinceFilter);
		} else {
			this.table.removeFilter("town.region.province");
			this.table.removeFilter("town.region");
			this.table.removeFilter("town");

		}
		if (this.mainLegalForm.getValue() != null) {
			this.mainLegalFormFilter = new Compare.Equal("legalForm.parent", this.mainLegalForm.getValue());
			this.table.addFilter("legalForm.parent", this.mainLegalFormFilter);
		} else {
			this.table.removeFilter("legalForm.parent");
			this.table.removeFilter("legalForm");
		}
		if (this.mainSector.getValue() != null) {
			this.mainSectorFilter = new Compare.Equal("sector.parent", this.mainSector.getValue());
			this.table.addFilter("sector.parent", this.mainSectorFilter);
		} else {
			this.table.removeFilter("sector.parent");
			this.table.removeFilter("sector");
		}

		if (this.fromRegistryResolutionDate.getValue() != null) {
			this.fromRegistryAcceptanceDateFilter = new Compare.GreaterOrEqual("xesRegistryResolutionDate", this.fromRegistryResolutionDate.getValue());
			this.table.addFilter("resolutionDate.from", this.fromRegistryAcceptanceDateFilter);
		} else {
			this.table.removeFilter("resolutionDate.from");
		}
		if (this.toRegistryResolutionDate.getValue() != null) {
			this.toRegistryAcceptanceDateFilter = new Compare.LessOrEqual("xesRegistryResolutionDate", this.toRegistryResolutionDate.getValue());
			this.table.addFilter("resolutionDate.to", this.toRegistryAcceptanceDateFilter);

		} else {
			this.table.removeFilter("resolutionDate.to");
		}
	}

}
