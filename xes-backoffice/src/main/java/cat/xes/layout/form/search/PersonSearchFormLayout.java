package cat.xes.layout.form.search;

import java.util.List;

import org.jamgo.layout.form.search.SearchModelFormLayout;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.exception.NoClassParentRelationDefinedException;
import org.jamgo.vaadin.ui.JmgTwoColumnFormLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.v7.data.Container.Filter;
import com.vaadin.v7.data.Property.ValueChangeEvent;
import com.vaadin.v7.data.Property.ValueChangeListener;
import com.vaadin.v7.data.fieldgroup.FieldGroup;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.data.util.filter.Compare;
import com.vaadin.v7.ui.CheckBox;
import com.vaadin.v7.ui.ComboBox;
import com.vaadin.v7.ui.TextField;

import cat.xes.model.Person;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Town;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PersonSearchFormLayout extends SearchModelFormLayout<Person> {

	private static final long serialVersionUID = 1L;

	private ComboBox region;
	private ComboBox province;
	private ComboBox town;

	private BeanItemContainer<Province> provinceBeanContainer = new BeanItemContainer<Province>(Province.class);
	private BeanItemContainer<Region> regionsBeanContainer = new BeanItemContainer<Region>(Region.class);
	private BeanItemContainer<Town> townsBeanContainer = new BeanItemContainer<Town>(Town.class);

	private Filter regionFilter;
	private Filter provinceFilter;

	@Override
	protected void initializeForm(Layout formLayout, FieldGroup binder) throws IllegalArgumentException, RepositoryForClassNotDefinedException {

		JmgTwoColumnFormLayout layout = (JmgTwoColumnFormLayout) formLayout;

		layout.addComponentToFirstColumn(binder.buildAndBind(this.messageSource.getMessage("entity.nif"), "nif"));
		((TextField) binder.getField("nif")).setNullRepresentation("");
		layout.addComponentToFirstColumn(binder.buildAndBind(this.messageSource.getMessage("entity.name"), "name"));
		((TextField) binder.getField("name")).setNullRepresentation("");
		layout.addComponentToFirstColumn(binder.buildAndBind(this.messageSource.getMessage("entity.active"), "active"));
		((CheckBox) binder.getField("active")).setValue(true);

		this.province = this.getComboBox("entity.province", "name.defaultText", true, this.provinceBeanContainer, 50);
		layout.addComponentToSecondColumn(this.province);

		this.region = this.getComboBox("entity.region", "name.defaultText", true, this.regionsBeanContainer, 50);
		layout.addComponentToSecondColumn(this.region);
		

		this.town = this.getComboBox("entity.town", "name.defaultText", true, this.townsBeanContainer, 50);
		layout.addComponentToSecondColumn(this.town);
		this.binder.bind(this.town, "town");
		this.region.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					Region region = (Region) event.getProperty().getValue();
					PersonSearchFormLayout.this.townsBeanContainer.removeAllItems();
					if (region != null) {
						List<Town> towns = PersonSearchFormLayout.this.datasourceServices.getAll(Town.class, region);
						PersonSearchFormLayout.this.townsBeanContainer.addAll(towns);
					}
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});
		this.province.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					Province province = (Province) event.getProperty().getValue();
					PersonSearchFormLayout.this.regionsBeanContainer.removeAllItems();
					PersonSearchFormLayout.this.townsBeanContainer.removeAllItems();
					if (province != null) {
						List<Region> regions = PersonSearchFormLayout.this.datasourceServices.getAll(Region.class, province);
						PersonSearchFormLayout.this.regionsBeanContainer.addAll(regions);
					}
				} catch (NoClassParentRelationDefinedException e) {
					e.printStackTrace();
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});

		layout.addComponentToSecondColumn(binder.buildAndBind(this.messageSource.getMessage("entity.othertown"), "otherTown"));
		((TextField) binder.getField("otherTown")).setNullRepresentation("");
		binder.getField("otherTown").setWidth(100, Unit.PERCENTAGE);

	}

	@Override
	protected void initializeContainers() throws RepositoryForClassNotDefinedException, NoClassParentRelationDefinedException {
		this.provinceBeanContainer.removeAllItems();
		this.provinceBeanContainer.addAll(this.datasourceServices.getAll(Province.class));

	}

	@Override
	protected void search() {
		super.search();

		if (this.region.getValue() != null) {
			this.regionFilter = new Compare.Equal("town.region", this.region.getValue());
			this.table.addFilter("town.region", this.regionFilter);
		} else {
			this.table.removeFilter("town.region");
			this.table.removeFilter("town");

		}
		if (this.province.getValue() != null) {
			this.provinceFilter = new Compare.Equal("town.region.province", this.province.getValue());
			this.table.addFilter("town.region.province", this.provinceFilter);
		} else {
			this.table.removeFilter("town.region.province");
			this.table.removeFilter("town.region");
			this.table.removeFilter("town");

		}
	}

}
