package cat.xes.login;

import org.jamgo.login.LoginUI;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.spring.annotation.SpringUI;

@SpringUI(path = "/login")
@Theme("xesTheme")
@Widgetset("cat.xes.Widgetset")
public class XESLoginUI extends LoginUI {

	private static final long serialVersionUID = 1L;

}
