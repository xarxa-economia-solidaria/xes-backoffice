package cat.xes.login;

import org.jamgo.login.LoginView;

import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

@SpringView(name = LoginView.NAME)
public class XESLoginView extends org.jamgo.login.LoginView {

	private static final long serialVersionUID = 1L;

	@Override
	protected HorizontalLayout createLoginHeaderLayout() {
		HorizontalLayout topBar = new HorizontalLayout();
		topBar.setWidth(100, Unit.PERCENTAGE);
		topBar.setStyleName("login-header");

		ThemeResource logoResource = new ThemeResource("images/xes-logo.png");
		Image logo = new Image(null, logoResource);
		logo.setStyleName("login-header-logo");
		Label applicationTitle = new Label("XES backoffice");
		applicationTitle.addStyleName("login-header-title");
		topBar.addComponents(logo, applicationTitle);
		topBar.setExpandRatio(logo, 1.0f);
		topBar.setExpandRatio(applicationTitle, 2.0f);
		topBar.setComponentAlignment(applicationTitle, Alignment.BOTTOM_CENTER);

		return topBar;
	}
}
