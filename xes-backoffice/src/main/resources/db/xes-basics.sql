insert into user(id,username,password,enabled,version) values (1,'user1', '$2a$11$hxzJhci3PfZMQnWls/LTdueqo/QWD2t9QBq7pLvLbFUwDRyaVF69K',1,0);

insert into role(id,rolename,version) values (1,'role_admin',0);
insert into role(id,rolename,version) values (2,'role_user',0);

insert into user_role(id_user,id_role) values (1, 1);

insert into language(id,name,language_code, country_code, version) values (1,'Català','ca','ES',0);
insert into language(id,name,language_code, country_code, version) values (2,'Castellano','es','ES',0);

insert into entity_state(id,version,name) values(1,0,'{texts:[{la:"ca",co:"ES",text:"Pendent"},{la:"es",co:"ES",text:"Pendiente"}]}');
insert into entity_state(id,version,name) values(2,0,'{texts:[{la:"ca",co:"ES",text:"Registrada"},{la:"es",co:"ES",text:"Registrada"}]}');
insert into entity_state(id,version,name) values(3,0,'{texts:[{la:"ca",co:"ES",text:"Declinada"},{la:"es",co:"ES",text:"Declinada"}]}');
insert into entity_state(id,version,name) values(4,0,'{texts:[{la:"ca",co:"ES",text:"Completada"},{la:"es",co:"ES",text:"Completada"}]}');

insert into associations(id,name,description,version) values (1,'XES','Xarxa d''Economia Solidaria',0);
insert into associations(id,name,description,version) values (3,'Coop57','Cooperativa de servicios financieros éticos y solidarios',0);
insert into associations(id,name,description,version) values (4,'B+S','Barcelona + Sostenible',0);