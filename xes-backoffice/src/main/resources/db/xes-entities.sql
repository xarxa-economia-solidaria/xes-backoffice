insert into USER (id,username, password, name, surname, enabled, email, id_language, version) values (2, 'F66380676', '$2a$11$hxzJhci3PfZMQnWls/LTdueqo/QWD2t9QBq7pLvLbFUwDRyaVF69K', 'Mercè', 'Botella Mas', 'true', 'merce.botella@eticom.coop', 1, 0);
insert into USER_ROLE (id_user, id_role) values (2, 2);
insert into entities(id,nif,name,shortname,description,web,year,address,cp,phone,email,id_town,id_legal_form,id_sector,id_xes_state,version,code) values (2, 'F66380676', 'Eticom Som Connexió', null, null, 'http://www.eticom.coop', '2015', 'Riu Llobregat, 47, Baixos', '08820', '615627328', 'merce.botella@eticom.coop', 105, 2, 58, 2, 0, '0041');

insert into USER (id,username, password, name, surname, enabled, email, id_language, version) values (3, 'G65935280', '$2a$11$hxzJhci3PfZMQnWls/LTdueqo/QWD2t9QBq7pLvLbFUwDRyaVF69K', 'Mariona ', 'L?pez Bosch', 'true', 'ml@superaccio.org', 1, 0);
insert into USER_ROLE (id_user, id_role) values (3, 2);
insert into entities(id,nif,name,shortname,description,web,year,address,cp,phone,email,id_town,id_legal_form,id_sector,id_xes_state,version,code) values (3, 'G65935280', 'Associaci? Superaccio', null, null, 'http://www.superaccio.org', null, null, null, '686413983', 'ml@superaccio.org', null, 5, 58, 2, 0, null);

insert into USER (id,username, password, name, surname, enabled, email, id_language, version) values (4, 'G65529661', '$2a$11$hxzJhci3PfZMQnWls/LTdueqo/QWD2t9QBq7pLvLbFUwDRyaVF69K', 'Anna ', 'Fuertes Serra', 'true', 'fundacioapipacam@fundacioapipacam.org', 1, 0);
insert into USER_ROLE (id_user, id_role) values (4, 2);
insert into entities(id,nif,name,shortname,description,web,year,address,cp,phone,email,id_town,id_legal_form,id_sector,id_xes_state,version,code) values (4, 'G65529661', 'Fundacio APIP_ACAM', null, null, 'http://www.fundacioapipacam.org', '1987', 'C/Paloma, 21-23', '08001', '933171614', 'fundacioapipacam@fundacioapipacam.org', 128, 7, 58, 1, 0, null);

insert into USER (id,username, password, name, surname, enabled, email, phone, id_language, version) values (5, 'G58433756', '$2a$11$hxzJhci3PfZMQnWls/LTdueqo/QWD2t9QBq7pLvLbFUwDRyaVF69K', 'Carla', 'Bosacoma Cortada', 'true', 'ss_admin@sidastudi.org', '555-666',1, 0);
insert into USER_ROLE (id_user, id_role) values (5, 2);
insert into entities(id,nif,name,shortname,description,web,year,address,cp,phone,email,id_town,id_legal_form,id_sector,id_xes_state,version,code) values (5, 'G58433756', 'SIDA STUDI', null, 'promoció de la salut sexual i la prevenció de riscos associa', 'http://www.sidastudi.org', '1987', 'C/ Emília Coranty, 5-9 (Complex Can Ricart)', '08018', '932681484', 'ss_admin@sidastudi.org', 128, 5, 58,1, 0, '0039');
insert into membership (id_entity, id_association) values (5, 3);

insert into person(id,version,nif,name,surnames,birth_date,address,cp,id_town,email,phone,bank_account_owner,bank_account_iban,quote,voluntary_quote,id_state) values (1,0,'30690245B','Aritz1','Samaniego1',parsedatetime('14-02-1982', 'dd-MM-yyyy'),'Joan Güell','08014',128,'aritz1.samaniego1@jamgo.es','555','Aritz1 Samaniego1','555444333','30','50',1);
insert into person(id,version,nif,name,surnames,birth_date,address,cp,id_town,email,phone,bank_account_owner,bank_account_iban,quote,voluntary_quote,id_state) values (2,0,'30690246B','Aritz2','Samaniego2',parsedatetime('14-02-1982', 'dd-MM-yyyy'),'Joan Güell','08014',128,'aritz2.samaniego2@jamgo.es','555','Aritz2 Samaniego2','555444333','30','50',2);

