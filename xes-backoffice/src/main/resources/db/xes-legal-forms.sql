INSERT INTO legal_forms (ID,version,id_parent,name) VALUES 
(13,0,NULL,'{texts:[{la:"ca",co:"ES",text:"Cooperativa"},{la:"es",co:"ES",text:"Cooperativa"}]}')
,(14,0,NULL,'{texts:[{la:"ca",co:"ES",text:"Societat Laboral"},{la:"es",co:"ES",text:"Societat Laboral"}]}')
,(15,0,NULL,'{texts:[{la:"ca",co:"ES",text:"No Lucratives"},{la:"es",co:"ES",text:"No Lucratives"}]}')
,(16,0,NULL,'{texts:[{la:"ca",co:"ES",text:"Mercantils"},{la:"es",co:"ES",text:"Mercantils"}]}')
,(17,0,NULL,'{texts:[{la:"ca",co:"ES",text:"Col·legis professionals"},{la:"es",co:"ES",text:"Col·legis professionals"}]}')
,(18,0,NULL,'{texts:[{la:"ca",co:"ES",text:"Administració pública"},{la:"es",co:"ES",text:"Administració pública"}]}')
,(19,0,NULL,'{texts:[{la:"ca",co:"ES",text:"Centres d''ensenyament"},{la:"es",co:"ES",text:"Centres d''ensenyament"}]}')
,(11,0,17,'{texts:[{la:"ca",co:"ES",text:"Col·legis professionals"},{la:"es",co:"ES",text:"Col·legis professionals"}]}')
,(12,0,18,'{texts:[{la:"ca",co:"ES",text:"Administració pública"},{la:"es",co:"ES",text:"Administració pública"}]}')
,(20,0,13,'{texts:[{la:"ca",co:"ES",text:"Cooperativa de crèdit"},{la:"es",co:"ES",text:"Cooperativa de crèdit"}]}')
;
INSERT INTO legal_forms (ID,version,id_parent,name) VALUES 
(1,0,13,'{texts:[{la:"ca",co:"ES",text:"Cooperativa de treball associat"},{la:"es",co:"ES",text:"Cooperativa de treball associat"}]}')
,(2,0,13,'{texts:[{la:"ca",co:"ES",text:"Cooperativa de consumidors i usuaris"},{la:"es",co:"ES",text:"Cooperativa de consumidors i usuaris"}]}')
,(3,0,13,'{texts:[{la:"ca",co:"ES",text:"Cooperativa de serveis"},{la:"es",co:"ES",text:"Cooperativa de serveis"}]}')
,(4,0,13,'{texts:[{la:"ca",co:"ES",text:"Cooperativa de segon grau"},{la:"es",co:"ES",text:"Cooperativa de segon grau"}]}')
,(5,0,15,'{texts:[{la:"ca",co:"ES",text:"Associació"},{la:"es",co:"ES",text:"Associació"}]}')
,(6,0,15,'{texts:[{la:"ca",co:"ES",text:"Associació de segon grau"},{la:"es",co:"ES",text:"Associació de segon grau"}]}')
,(7,0,15,'{texts:[{la:"ca",co:"ES",text:"Fundació"},{la:"es",co:"ES",text:"Fundació"}]}')
,(8,0,14,'{texts:[{la:"ca",co:"ES",text:"Societat limitada laboral"},{la:"es",co:"ES",text:"Societat limitada laboral"}]}')
,(9,0,16,'{texts:[{la:"ca",co:"ES",text:"Societat de responsabilitat limitada"},{la:"es",co:"ES",text:"Societat de responsabilitat limitada"}]}')
,(10,0,15,'{texts:[{la:"ca",co:"ES",text:"Federació"},{la:"es",co:"ES",text:"Federació"}]}')
;
INSERT INTO legal_forms (ID,version,id_parent,name) VALUES 
(21,0,13,'{texts:[{la:"ca",co:"ES",text:"Cooperativa mixta"},{la:"es",co:"ES",text:"Cooperativa mixta"}]}')
,(22,0,13,'{texts:[{la:"ca",co:"ES",text:"Grup cooperatiu"},{la:"es",co:"ES",text:"Grup cooperatiu"}]}')
,(23,0,16,'{texts:[{la:"ca",co:"ES",text:"Societat Anònima"},{la:"es",co:"ES",text:"Societat Anònima"}]}')
,(24,0,16,'{texts:[{la:"ca",co:"ES",text:"Societat Civil particular"},{la:"es",co:"ES",text:"Societat Civil particular"}]}')
,(25,0,14,'{texts:[{la:"ca",co:"ES",text:"Societat anòmina laboral"},{la:"es",co:"ES",text:"Societat anòmina laboral"}]}')
,(26,0,19,'{texts:[{la:"ca",co:"ES",text:"Centres d''ensenyament"},{la:"es",co:"ES",text:"Centres d''ensenyament"}]}')
;