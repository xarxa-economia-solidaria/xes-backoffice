//package cat.xes.api.controller.test;
//
//import static org.junit.Assert.assertNotNull;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
//
//import java.io.IOException;
//import java.util.Arrays;
//
//import javax.ws.rs.core.Application;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.mock.http.MockHttpOutputMessage;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.web.context.WebApplicationContext;
//
//import cat.xes.model.Entity;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//public class RegisterTest {
//
//	private MockMvc mockMvc;
//
//	@Autowired
//	private WebApplicationContext webApplicationContext;
//
//	private HttpMessageConverter mappingJackson2HttpMessageConverter;
//
//	@Autowired
//	void setConverters(HttpMessageConverter<?>[] converters) {
//
//		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
//			.filter(hmc -> hmc instanceof 	 MappingJackson2HttpMessageConverter)
//			.findAny()
//			.orElse(null);
//
//		assertNotNull("the JSON message converter must not be null",
//			this.mappingJackson2HttpMessageConverter);
//	}
//
//	@Before
//	public void setup() throws Exception {
//		this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
//	}
//
//	@Test
//	public void entityRegistryRequest() throws Exception {
//		Entity entity = new Entity();
//		this.mockMvc.perform(post("/api/entities/register").content(this.json(entity)))
//			.andExpect(status().isOk());
//	}
//
//	protected String json(Object o) throws IOException {
//		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
//		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
//		return mockHttpOutputMessage.getBodyAsString();
//	}
//
//}
