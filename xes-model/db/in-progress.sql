ALTER TABLE entities ADD is_xes BOOL DEFAULT false NOT NULL;
update entities set is_xes = id in (select id_entity from membership m2 where m2.id_association  = 1);

ALTER TABLE person ADD is_active TINYINT DEFAULT 0 NOT NULL;
