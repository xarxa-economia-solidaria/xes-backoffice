begin;

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F66929688','ARQBAG SCCL',null,'Crta Vella, 37','8110',257,'932740627','andromines@andromines.net',null,5,'http://www.andromines.net',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Montse/Natalia Marco','pass1','F66929688',1,'xes','Ana Montoro 647735334','administracio@andromines.net, coordinacio@andromines.net',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G66850769','Associació MAP13',50,'C/ Casp, 43 baixos','8010',128,null,'info@calidoscoop.coop',null,1,'http://www.calidoscoop.coop',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1','G66850769',1,'xes','670537525','pere@calidoscoop.coop,',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'en procés constitució (no tenim NIF encara)','Bruna Films',61,'C/ Mozart, 2','8012',128,'931650770','correu@cos.coop',null,1,'http://www.cos.coop',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Jordi Vinade 687965366','pass1','en procés constitució (no tenim NIF encara)',1,'xes',null,'jordi@cos.coop,laura@cos.coop',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact,id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F59113696','Cegecop, S.Coop.C.Ltda ',27,'C/ Jocs Florals, ','8014',128,'678905871','economatcoop@gmail.com',null,1,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Diana o Marc','pass1','	',1,'xes',null,'jmelero39@gmail.com',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F66937319','Coodin, SCCL',32,'c/.Camp del Molí, 15','25280',734,'973483943','marti@dansasolsona.cat',null,null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Martí Prades','pass1','F66937319',1,'xes','658953078','info@dansasolsona.cat',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F66536145','CoopHalal',null,'Mossèn Jacint Verdaguer 46 ','8241',76,'93 873 1350 ','queralt.jorba@gmail.com',null,3,'http://www.fornjorba.com ',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Queralt Jorba Bayona ','pass1','F66536145',1,'xes','649 811 692 ',null,1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F65597072','E.I. SAMBUCUS, SCCL',null,'C.Doctor Aiguadé 18 bx','8003',128,'932690145','isabel.gvb@confavc.cat',null,1,'http://www.confavc.org  ',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Isabel Cabezas','pass1','F65597072',1,'xes',null,null,1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G63082135','EDPAC',null,'C/ Pau Claris 167 7a','8037',128,'934873947','info@koinosbcn.com',null,1,'http://www.koinosbcn.com',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Cathy Anaud','pass1','G63082135',1,'xes','932216274-677846686','arnaud.cathy@gmail.com',1);
--insert into membership(id_entity,id_association) values (1057,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1057;

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, --id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'49324583W','Greenevents',null,'Av. Ernest Lluch, 32','8302',180,'931 696 555','info@verkami.com','fakturo@verkami.com',1,'http://www.verkami.com',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Jonàs','pass1','49324583W',1,'xes',null,'jonas@verkami.com',1);
--insert into membership(id_entity,id_association) values (1218,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1218;

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F66288192','I-LabSo SCCL',null,'C/ Mossèn Joan Rebull, 11, 1r 3a','8340',197,'689063151-937593402','mariona.zj@esberla.cat,esberla@esberla.cat',null,null,'',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1','F66288192',1,'xes',null,'amendieta@esberla.cat,sergi.gv@esberla.cat',1);
insert into membership(id_entity,id_association) values (1047,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1047;

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F64229230','LaFundició',null,'c/Cermeño 7, baixos','8003',128,'616184105','aleksdufour@gmail.com','ester_retse@hotmail.com',1,'',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Laia','pass1','F64229230',1,'xes',null,'gruplasardineta@gmail.com',1);
--insert into membership(id_entity,id_association) values (1238,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1238;

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G66601972','MATRIU ',null,'Carrer de CAMPRODON, 3. BAIXOS (SEU SOCIAL, DIPUTACIÓ, 251, 5A PLANTA)','8012',128,'934590022','comunicacio@ecoserveis.net','administracio@ecoserveis.net',5,'',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 
--'USER_SEQ'),0,'Oriol','pass1','G66601972',1,'xes',null,null,1);
insert into membership(id_entity,id_association) values (1070,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1070;

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G66314097','NUS TEATRE',null,'Albigesos, 9 Pral 2a','8024',128,'93 009 06 53','sira@laclaracomunicacio.com',null,1001,'',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Marta','pass1','G66314097',1,'xes',null,null,1);
insert into membership(id_entity,id_association) values (1027,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1027;

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F 66931171','Ortiga grup cooperatiu sccl',null,'c/ Roger de Flor, 85','8013',128,'933027347','mireia.font@eltimbal.org','rut.bertran@eltimbal.org',null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1','F 66931171',1,'xes',null,'escola@eltimbal.org',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'F66368739','TRACTA''M S.C.C.L',null,'Rda Alfons X el savi, 176 ','8301',180,'696881164','info@aliancesostenibles.cat','Luz',null,'http://www.aliancesostenibles.cat',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1','F66368739',1,'xes',null,'info@greenevents.cat',1);
--insert into membership(id_entity,id_association) values (1211,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1211;

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G-66740739','VOLTA ARQUITECTES',null,'C/ Escorial. 6 3º 2ª','8024',128,'653394230','info.arqbag@gmail.com',null,1,'http://www.arqbag.com ',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Adrià Vilajoana/Simona','pass1','G-66740739',1,'xes',null,'avilajoana.arqbag@gmail.com ',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'XESEMPORDA','XES Empordà',null,null,null,null,null,null,null,null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1',null,1,'xes',null,null,1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'XESGARRAF','XES Garraf',null,null,null,null,null,null,null,null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1',null,1,'xes',null,null,1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'XESGIRONA','XES Girona',null,null,null,517,null,'nanolola@icloud.com',null,null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1',null,1,'xes',null,null,1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'XESBLANES','XES La Selva Maritima ( Blanes )',null,null,null,467,null,'sales.fava.lluis@gmail.com',null,null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1',null,1,'xes',null,'gsanz6@gmail.com',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'XESLLEIDA','XES Terres de LLeida ',null,null,null,269,null,'javirolan@gmail.com;xesterrassa@gmail.com',null,null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1',null,1,'xes',null,'joanpigonz@gmail.com',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'XESVALLES','XES Vallès',null,null,null,328,'699747307','montse.boher@gmail.com',null,null,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,null,'pass1',null,1,'xes',null,'mireia.vehi@gmail.com',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'ARQBAG','ARQBAG SCCL',null,'Crta Vella, 37','8110',257,'932740627','andromines@andromines.net',null,5,'http://www.andromines.net',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Montse/Natalia Marco','pass1',null,1,'xes','Ana Montoro 647735334','administracio@andromines.net, coordinacio@andromines.net',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G61600037','IAC',null,'Via Laietana 57 4t 3a','8003',128,'933173151','sindicat@catac.cat','organitzacio@catac.cat',5,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'x','pass1','G61600037',1,'xes','x','marsan81@gmail.com',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G08858193','FCCUC',null,'C/ Premià 15 2ª planta ','8014',128,' ','cooperativesdeconsum@fccuc.coop','quim.sicilia@fccuc.coop',10,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'x','pass1','G08858193',1,'xes','x','m.antonia@fccuc.coop',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
--update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
--insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G62015789','FCTC',null,'C/ Premià 15, 1ª planta','8014',128,'933188162','info@cooperativestreball.coop;nballesteros@cronda.coop','guillem@cooperativestreball.coop',10,'http://www.cooperativestreball.coop',NOW(),NOW(),1,2);	
--insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'x','pass1','G62015789',1,'xes','x','carme@cooperativestreball.coop',1);
--insert into membership(id_entity,id_association) values (1158,1);
update entities set active = 1, id_xes_state = 2,xes_registry_request_date = NOW(), xes_registry_resolution_date = NOW() where id = 1158;

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G62311501','ATTAC CATALUNYA',null,'Avinguda Diagonal 329','8009',128,'934250385','attac@attac-catalunya.org;simpatitzants-attac@mail.pangea.org','konsfer1@telefonica.net',5,'http://www.attac-catalunya.org',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'x','pass1','G62311501',1,'xes','x','cgefaell@hotmail.com,snollab@hotmail.com',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G63920706','Fundació Unió Cooperadors de Mataró',null,'C/ Riera, 13-15 2º 2ª','8302',180,null,'angelbolta@gmail.com','mguillen@cooperadorsdemataro.coop',7,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'x','pass1','G63920706',1,'xes','x','mlamata@ajmataro.cat',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G65875031','ADBdT (Associació pel Desenvolupament dels Bancs de Temps)',null,'Carrer de la Providència, 42 (Hotel Entitats)','8024',128,'653384286','sseerrggii@gmail.com','jmaciae@gmail.com',5,'',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'x','pass1','G65875031',1,'xes','x','adbdt.info@gmail.com',1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'ENTITY_SEQ';
update sequence_table set SEQ_COUNT = SEQ_COUNT + 1 where SEQ_NAME = 'USER_SEQ';
insert into entities(id, nif, name, id_sector, address, cp, id_town, phone, email, administration_contact, id_legal_form,web,xes_registry_request_date,xes_registry_resolution_date,active,id_xes_state) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),'G08850299','Federació d''Associacions de Mares i Pares d''Alumnes de Catalunya',null,'Carrer de Cartagena, 245, àtic','8025',128,'934357686','direccio@fapac.cat',null,5,'http://www.fapac.cat',NOW(),NOW(),1,2);	
insert into user (id, version,name, password,username,id_language,dtype,phone,email,enabled) values((select SEQ_COUNT from sequence_table where SEQ_NAME = 'USER_SEQ'),0,'Andreu/Gloria ','pass1','G08850299',1,'xes',null,null,1);
insert into membership(id_entity,id_association) values ((select SEQ_COUNT from sequence_table where SEQ_NAME = 'ENTITY_SEQ'),1);

commit;
