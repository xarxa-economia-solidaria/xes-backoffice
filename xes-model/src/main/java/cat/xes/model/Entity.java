
package cat.xes.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.CacheIndex;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.portable.Portable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cat.xes.model.export.EntityExporter;

@javax.persistence.Entity
@Table(name = "entities")
@DiscriminatorValue(value = Entity.EVALUABLE_TYPE_ID)
@Cacheable(value = false)
@Portable(exporter = EntityExporter.class)
public class Entity extends Evaluable {

	private static final long serialVersionUID = 1L;

	public static final String EVALUABLE_TYPE_ID = "1";

	@Id
	@TableGenerator(name = "ENTITY_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "ENTITY_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ENTITY_ID_GEN")
	private Long id;

	@Column(name = "NIF", nullable = false)
	@CacheIndex
	private String nif;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "shortname")
	private String shortName;

	@Column(name = "description")
	private String description;

	@Column(name = "xes_description")
	private LocalizedString xesDescription;

	@Column
	private String web;

	@Column(name = "creationyear")
	private String year;

	@Column
	private String address;

	@Column
	private String cp;

	@Column(nullable = false)
	private String phone;

	@Column(nullable = false)
	private String email;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_user")
	private XesUser user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_town")
	private Town town;

	@Column(name = "other_town")
	private String otherTown;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_legal_form")
	private LegalForm legalForm;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_sector")
	private Sector sector;

	@Column(name = "pamapam_url")
	private String pamAPamUrl;

	@ManyToOne
	@JoinColumn(name = "id_xes_state", nullable = false)
	private EntityState xesState;

	@Column(name = "code")
	private String code;

	@Column(name = "active")
	private boolean active;
	
	@Column(name = "xes_active")
	private boolean xesActive;
	
	@Column(name = "is_xes")
	private boolean xes;

	@Column(name = "allow_xes_info")
	private boolean allowXesInfo;

	@Column(name = "partner_number")
	private int partnerNumber;

	@Basic(fetch = FetchType.LAZY)
	@Lob
	private byte[] logo;

	@Column(name = "xes_registry_request_date")
	@Temporal(TemporalType.DATE)
	private Date xesRegistryRequestDate;

	@Column(name = "xes_registry_resolution_date")
	@Temporal(TemporalType.DATE)
	private Date xesRegistryResolutionDate;

	@Column(name = "bank_account_owner")
	private String bankAccountOwner;
	@Column(name = "bank_account_iban")
	private String bankAccountIban;
	@Column(name = "administration_contact")
	private String administrationContact;

	@Column(name = "xes_collaborator")
	private boolean collaborator;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Entity() {
		this.user = new XesUser();
	}

	public String getNif() {
		return this.nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String creationYear) {
		this.year = creationYear;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCp() {
		return this.cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public XesUser getUser() {
		return this.user;
	}

	public void setUser(XesUser user) {
		this.user = user;
	}

	public Region getRegion() {
		if (this.town != null) {
			return this.town.getRegion();
		}
		return null;
	}

	public Town getTown() {
		return this.town;
	}

	public void setTown(Town town) {
		this.town = town;
	}

	public LegalForm getLegalForm() {
		return this.legalForm;
	}

	public void setLegalForm(LegalForm legalForm) {
		this.legalForm = legalForm;
	}

	public Sector getSector() {
		return this.sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public EntityState getXesState() {
		return this.xesState;
	}

	public void setXesState(EntityState xesState) {
		this.xesState = xesState;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public Province getProvince() {
		if (this.getRegion() != null) {
			return this.getRegion().getProvince();
		}
		return null;
	}

	public String getOtherTown() {
		return this.otherTown;
	}

	public void setOtherTown(String otherTown) {
		this.otherTown = otherTown;
	}

	public byte[] getLogo() {
		return this.logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public String getPamAPamUrl() {
		return this.pamAPamUrl;
	}

	public void setPamAPamUrl(String pamAPamUrl) {
		this.pamAPamUrl = pamAPamUrl;
	}

	public int getPartnerNumber() {
		return this.partnerNumber;
	}

	public void setPartnerNumber(int partnerNumber) {
		this.partnerNumber = partnerNumber;
	}

	public boolean isAllowXesInfo() {
		return this.allowXesInfo;
	}

	public void setAllowXesInfo(boolean allowXesInfo) {
		this.allowXesInfo = allowXesInfo;
	}

	public LocalizedString getXesDescription() {
		return this.xesDescription;
	}

	public void setXesDescription(LocalizedString xesDescription) {
		this.xesDescription = xesDescription;
	}

	public Date getXesRegistryRequestDate() {
		return this.xesRegistryRequestDate;
	}

	public void setXesRegistryRequestDate(Date xesRegistryRequestDate) {
		this.xesRegistryRequestDate = xesRegistryRequestDate;
	}

	public Date getXesRegistryResolutionDate() {
		return this.xesRegistryResolutionDate;
	}

	public void setXesRegistryResolutionDate(Date xesRegistryResolutionDate) {
		this.xesRegistryResolutionDate = xesRegistryResolutionDate;
	}

	public String getBankAccountOwner() {
		return this.bankAccountOwner;
	}

	public void setBankAccountOwner(String bankAccountOwner) {
		this.bankAccountOwner = bankAccountOwner;
	}

	public String getBankAccountIban() {
		return this.bankAccountIban;
	}

	public void setBankAccountIban(String bankAccountIban) {
		this.bankAccountIban = bankAccountIban;
	}

	public String getAdministrationContact() {
		return this.administrationContact;
	}

	public void setAdministrationContact(String administrationContact) {
		this.administrationContact = administrationContact;
	}

	public boolean isCollaborator() {
		return this.collaborator;
	}

	public void setCollaborator(boolean collaborator) {
		this.collaborator = collaborator;
	}

	public boolean isXesActive() {
		return xesActive;
	}

	public void setXesActive(boolean xesActive) {
		this.xesActive = xesActive;
	}

	public boolean isXes() {
		return xes;
	}

	public void setXes(boolean xes) {
		this.xes = xes;
	}

}
