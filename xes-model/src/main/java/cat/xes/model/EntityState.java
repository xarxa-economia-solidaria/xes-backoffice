package cat.xes.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.entity.Model;

@Entity
@Table(name = "entity_state")
public class EntityState extends Model {

	private static final long serialVersionUID = 1L;

	public enum State {
		Pending(1L), Registered(2L), Declined(3L), Completed(4L);

		private Long id;

		State(Long id) {
			this.id = id;
		}

		public Long getId() {
			return this.id;
		}
	};

	@Id
	private Long id;

	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return ((obj instanceof EntityState) && ((EntityState) obj).getName().equals(this.getName()))
			|| ((obj instanceof Long) && ((Long) obj).equals(this.getId()));
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
