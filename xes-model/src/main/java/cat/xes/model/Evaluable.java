package cat.xes.model;

import javax.persistence.Cacheable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.jpa.entity.Model;

@javax.persistence.Entity
@Table(name = "evaluable")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "id_evaluable_type")
@Cacheable(value = false)
public abstract class Evaluable extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "EVALUABLE_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "EVALUABLE_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "EVALUABLE_ID_GEN")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_evaluable_type")
	private EvaluableType type;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public EvaluableType getType() {
		return this.type;
	}

	public void setType(EvaluableType type) {
		this.type = type;
	}
}