package cat.xes.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "evaluable_type")
@Cacheable(value = false)
public class EvaluableType extends org.jamgo.model.jpa.entity.Model {

	private static final long serialVersionUID = 1L;

	public static final Long ENTITY_TYPE_ID = 1L;
	public static final Long PROJECT_TYPE_ID = 2L;

	public enum Type {
		Entity, Project
	};

	@Id
	private Long id;

	@Column(name = "name")
	@Enumerated(EnumType.STRING)
	private Type type;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}
}