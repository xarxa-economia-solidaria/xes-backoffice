package cat.xes.model;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.jpa.converter.LocalizedAttributeConverter;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.entity.Model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "legal_forms")
public class LegalForm extends Model {

	public static final Long NO_LUCRATIVES_ID = 15L;
	public static final Long COLLEGIS_PROFESSIONALS = 17L;
	public static final Long ADMINISTRACIO_PUBLICA = 18L;

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "LEGALFORM_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "LEGALFORM_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "LEGALFORM_ID_GEN")
	private Long id;

	@JsonManagedReference("legal_form_parent")
	@ManyToOne
	@JoinColumn(name = "id_parent")
	private LegalForm parent;

	@JsonBackReference("legal_form_parent")
	@OneToMany(mappedBy = "parent")
	private List<LegalForm> legalForms;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public List<LegalForm> getLegalForms() {
		return this.legalForms;
	}

	public void setLegalForms(List<LegalForm> legalForms) {
		this.legalForms = legalForms;
	}

	public LegalForm getParent() {
		return this.parent;
	}

	public void setParent(LegalForm parent) {
		this.parent = parent;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
