package cat.xes.model;

import javax.persistence.MappedSuperclass;

import org.jamgo.model.jpa.entity.Language;

import cat.xes.model.literal.LiteralExtended;

@MappedSuperclass
public abstract class LocalizedExtendedModel<T extends LiteralExtended<?>> extends LocalizedModel<T> {

	private static final long serialVersionUID = 1L;

	public String getDescription(Language lang) {
		if (lang != null) {
			return this.getLiteralsMap().get(lang).getDescription();
		}
		return this.getDefaultDescription();
	}

	public String getDefaultDescription() {
		return this.getLiterals().get(0).getDescription();
	}
}
