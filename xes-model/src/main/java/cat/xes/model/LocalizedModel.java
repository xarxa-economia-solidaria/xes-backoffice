package cat.xes.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKey;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.jamgo.model.jpa.entity.Language;
import org.jamgo.model.jpa.entity.Model;
import org.springframework.util.CollectionUtils;

import cat.xes.model.literal.Literal;

@MappedSuperclass
@Deprecated
public abstract class LocalizedModel<T extends Literal<?>> extends Model {

	private static final long serialVersionUID = 1L;

	@MapKey(name = "language")
	@JoinColumn(name = "id_model", insertable = false, updatable = false)
	private Map<Language, T> literalsMap;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_model", insertable = true, updatable = true)
	private List<T> literals;

	public LocalizedModel() {
	}

	protected Map<Language, T> getLiteralsMap() {
		return this.literalsMap;
	}

	@Override
	public void setId(Long id) {
		if (this.literals != null) {
			for (T literal : this.literals) {
				literal.setIdModel(id);
			}
		}
	}

	public void addLiteral(T literal) {
		if (this.literalsMap == null) {
			this.literalsMap = new HashMap<Language, T>();
		}
		this.literalsMap.put(literal.getLanguage(), literal);

		if (this.literals == null) {
			this.literals = new ArrayList<T>();
		}
		this.literals.add(literal);
	}

	public void setLiteralsMap(Map<Language, T> literalsMap) {
		this.literalsMap = literalsMap;
	}

	public List<T> getLiterals() {
		if (this.literals == null) {
			this.literals = new ArrayList<T>();
		}
		return this.literals;
	}

	public T getLiteral(Language language) {
		return this.literalsMap.get(language);
	}

	public static class Comparator<L extends Literal<?>, Y extends Serializable> implements java.util.Comparator<LocalizedModel<L>> {
		private final Language language;

		public Comparator(Language language) {
			this.language = language;
		}

		@Override
		public int compare(LocalizedModel<L> model1, LocalizedModel<L> model2) {
			String name1 = model1.getName(this.language);
			String name2 = model2.getName(this.language);
			return name1.compareTo(name2);
		}
	}

	@Override
	public String toString() {
		return this.getDefaultName();
	}

	public String getDefaultName() {
		if (CollectionUtils.isEmpty(this.getLiterals())) {
			return super.toString();
		}
		return this.getLiterals().get(0).getName();
	}

	public String getName(Language lang) {
		if (lang != null) {
			return this.getLiteralsMap().get(lang).getName();
		}
		return this.getDefaultName();
	}
}
