package cat.xes.model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = { ModelConfiguration.class })
public class ModelConfiguration {

}
