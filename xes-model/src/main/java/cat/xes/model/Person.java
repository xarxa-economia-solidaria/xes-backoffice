package cat.xes.model;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.jpa.entity.Model;
import org.jamgo.model.jpa.portable.Portable;

import cat.xes.model.export.PersonExporter;

@javax.persistence.Entity
@Table(name = "person")
@Portable(exporter = PersonExporter.class)
@Cacheable(value = false)
public class Person extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "PERSON_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "PERSON_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PERSON_ID_GEN")
	private Long id;

	private String nif;
	private String name;
	private String surnames;
	@Column(name = "birth_date")
	private Date birthDate;
	private String address;
	private String cp;
	@ManyToOne
	@JoinColumn(name = "id_town")
	private Town town;
	@Column(name = "other_town")
	private String otherTown;
	private String email;
	private String phone;
	@Column(name = "bank_account_owner")
	private String bankAccountOwner;
	@Column(name = "bank_account_iban")
	private String bankAccountIban;
	private String quote;
	@Column(name = "voluntary_quote")
	private String voluntaryQuote;

	@ManyToOne
	@JoinColumn(name = "id_state", nullable = false)
	private EntityState state;
	@Column(name = "registry_request_date")
	private Date registryRequestDate;
	@Column(name = "registry_resolution_date")
	private Date registryResolutionDate;
	
	@Column(name = "is_active")
	private boolean active;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNif() {
		return this.nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurnames() {
		return this.surnames;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCp() {
		return this.cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public Town getTown() {
		return this.town;
	}

	public void setTown(Town town) {
		this.town = town;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBankAccountOwner() {
		return this.bankAccountOwner;
	}

	public void setBankAccountOwner(String bankAccountOwner) {
		this.bankAccountOwner = bankAccountOwner;
	}

	public String getBankAccountIban() {
		return this.bankAccountIban;
	}

	public void setBankAccountIban(String bankAccountIban) {
		this.bankAccountIban = bankAccountIban;
	}

	public String getQuote() {
		return this.quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public String getVoluntaryQuote() {
		return this.voluntaryQuote;
	}

	public void setVoluntaryQuote(String voluntaryQuote) {
		this.voluntaryQuote = voluntaryQuote;
	}

	public String getOtherTown() {
		return this.otherTown;
	}

	public void setOtherTown(String otherTown) {
		this.otherTown = otherTown;
	}

	public Date getRegistryRequestDate() {
		return this.registryRequestDate;
	}

	public void setRegistryRequestDate(Date registryRequestDate) {
		this.registryRequestDate = registryRequestDate;
	}

	public Date getRegistryResolutionDate() {
		return this.registryResolutionDate;
	}

	public void setRegistryResolutionDate(Date registryResolutionDate) {
		this.registryResolutionDate = registryResolutionDate;
	}

	public EntityState getState() {
		return this.state;
	}

	public void setState(EntityState state) {
		this.state = state;
	}

	public Region getRegion() {
		if (this.town != null) {
			return this.town.getRegion();
		}
		return null;
	}

	public Province getProvince() {
		if (this.getRegion() != null) {
			return this.getRegion().getProvince();
		}
		return null;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
