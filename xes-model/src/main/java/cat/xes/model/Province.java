package cat.xes.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.jpa.converter.LocalizedAttributeConverter;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.entity.Model;

import com.fasterxml.jackson.annotation.JsonBackReference;

@javax.persistence.Entity
@Table(name = "provinces")
public class Province extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "PROVINCE_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "PROVINCE_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PROVINCE_ID_GEN")
	private Long id;

	@JsonBackReference("province")
	@OneToMany(mappedBy = "province", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Region> regions;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
//		super.setId(id);
	}

	public List<Region> getRegions() {
		return this.regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
