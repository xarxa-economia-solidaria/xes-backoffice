package cat.xes.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.jpa.converter.LocalizedAttributeConverter;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.entity.Model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "regions")
public class Region extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "REGION_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "REGION_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "REGION_ID_GEN")
	private Long id;

	@JsonBackReference("region")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_region")
	private List<Town> towns;

	@JsonManagedReference("town")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_province")
	private Province province;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
//		super.setId(id);
	}

	public List<Town> getTowns() {
		return this.towns;
	}

	public void setTowns(List<Town> towns) {
		this.towns = towns;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
