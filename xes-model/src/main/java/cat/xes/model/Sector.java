package cat.xes.model;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.jpa.converter.LocalizedAttributeConverter;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.entity.Model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "sectors")
public class Sector extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "SECTOR_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "SECTOR_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SECTOR_ID_GEN")
	private Long id;

	@JsonBackReference("sector-parent")
	@OneToMany(mappedBy = "parent")
	private List<Sector> sectors;

	@JsonManagedReference("sector-parent")
	@ManyToOne
	@JoinColumn(name = "id_parent")
	private Sector parent;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	public Sector() {
		super();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Sector getParent() {
		return this.parent;
	}

	public void setParent(Sector parent) {
		this.parent = parent;
	}

	public List<Sector> getSectors() {
		return this.sectors;
	}

	public void setSectors(List<Sector> sectors) {
		this.sectors = sectors;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
