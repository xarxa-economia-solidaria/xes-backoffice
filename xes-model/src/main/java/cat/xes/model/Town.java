package cat.xes.model;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.jpa.converter.LocalizedAttributeConverter;
import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.entity.Model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "towns")
public class Town extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "TOWN_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "TOWN_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TOWN_ID_GEN")
	private Long id;

	@JsonManagedReference("region")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_region")
	private Region region;

	@Convert(converter = LocalizedAttributeConverter.class)
	private LocalizedString name;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(LocalizedString name) {
		this.name = name;
	}
}
