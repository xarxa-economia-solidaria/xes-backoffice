package cat.xes.model;

import javax.persistence.Cacheable;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@javax.persistence.Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = { "username" }))
@Cacheable(value = false)
public class XesUser extends org.jamgo.model.jpa.entity.User {

	private static final long serialVersionUID = 1L;

	@OneToOne(optional = true, fetch = FetchType.LAZY, mappedBy = "user")
	private Entity entity;

	private String phone;

	public Entity getEntity() {
		return this.entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
