package cat.xes.model.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cat.xes.model.Entity;
import cat.xes.model.EntityState;
import cat.xes.model.LegalForm;
import cat.xes.model.Sector;

public interface EntitiesDao extends JpaRepository<Entity, Long> {

	public Entity findByNif(String nif);

	public Entity findByCode(String code);

	public Set<Entity> findByXes(boolean isXes);
	
	public Set<Entity> findByXesState(EntityState state);

	public Set<Entity> findBySectorAndXesStateAndLegalForm(Sector sector, EntityState state, LegalForm legalForm);
	
	@Query("select e from Entity e where e.xesRegistryRequestDate is not null")
	public Set<Entity> getAll();

	@Query("select e.logo from Entity e where e.nif = :nif")
	public byte[] getLogo(@Param("nif") String nif);
}
