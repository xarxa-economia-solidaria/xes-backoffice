package cat.xes.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.EntityState;

public interface EntityStateDao extends JpaRepository<EntityState, Long> {

}
