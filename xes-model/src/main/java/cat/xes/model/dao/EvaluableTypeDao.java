package cat.xes.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.EvaluableType;

public interface EvaluableTypeDao extends JpaRepository<EvaluableType, Long> {

	public EvaluableType findByType(EvaluableType.Type type);
}
