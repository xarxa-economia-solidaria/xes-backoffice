package cat.xes.model.dao;

import javax.annotation.PostConstruct;

import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cat.xes.model.Entity;
import cat.xes.model.EntityState;
import cat.xes.model.LegalForm;
import cat.xes.model.Person;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;
import cat.xes.model.XesUser;

@Repository
public class JpaRepositoryFactory extends org.jamgo.model.jpa.dao.JpaRepositoryFactory {

	@Autowired
	private SectorsDao sectorDao;
	@Autowired
	private EntitiesDao entitiesDao;
	@Autowired
	private LegalFormsDao legalFormsDao;
	@Autowired
	private RegionsDao regionsDao;
	@Autowired
	private UsersDao usersDao;
	@Autowired
	private TownsDao townsDao;
	@Autowired
	private ProvincesDao provincesDao;
	@Autowired
	private EntityStateDao entityStateDao;
	@Autowired
	private PersonDao personDao;

	@Override
	@PostConstruct
	public void init() {
		super.init();
		this.jpaRepositoryMap.put(Province.class, this.provincesDao);
		this.jpaRepositoryMap.put(Town.class, this.townsDao);
		this.jpaRepositoryMap.put(XesUser.class, this.usersDao);
		this.jpaRepositoryMap.put(Region.class, this.regionsDao);
		this.jpaRepositoryMap.put(LegalForm.class, this.legalFormsDao);
		this.jpaRepositoryMap.put(Entity.class, this.entitiesDao);
		this.jpaRepositoryMap.put(Sector.class, this.sectorDao);
		this.jpaRepositoryMap.put(EntityState.class, this.entityStateDao);
		this.jpaRepositoryMap.put(Person.class, this.personDao);
	}

	@Override
	public JpaRepository<?, ?> getRepository(Class<?> clazz) throws RepositoryForClassNotDefinedException {
		try {
			return super.getRepository(clazz);
		} catch (RepositoryForClassNotDefinedException e) {
			// do nothing
		}

		if (this.jpaRepositoryMap.containsKey(clazz)) {
			return this.jpaRepositoryMap.get(clazz);
		}
		throw new RepositoryForClassNotDefinedException(clazz.getName());
	}
}
