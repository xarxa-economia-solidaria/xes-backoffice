package cat.xes.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.LegalForm;

public interface LegalFormsDao extends JpaRepository<LegalForm, Long> {

	List<LegalForm> findByParent(LegalForm parent);
}
