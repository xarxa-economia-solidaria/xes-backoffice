package cat.xes.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.EntityState;
import cat.xes.model.Person;

public interface PersonDao extends JpaRepository<Person, Long> {

	public List<Person> findByState(EntityState state);

	public Person findByNif(String nif);

}
