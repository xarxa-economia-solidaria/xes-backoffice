package cat.xes.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.Province;

public interface ProvincesDao extends JpaRepository<Province, String> {

}
