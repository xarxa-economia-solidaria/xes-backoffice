package cat.xes.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.Province;
import cat.xes.model.Region;

public interface RegionsDao extends JpaRepository<Region, String> {

	public List<Region> findByProvince(Province province);
}
