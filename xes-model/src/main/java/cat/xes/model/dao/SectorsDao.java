package cat.xes.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.Sector;

public interface SectorsDao extends JpaRepository<Sector, Long> {

	List<Sector> findByParent(Sector parent);
}
