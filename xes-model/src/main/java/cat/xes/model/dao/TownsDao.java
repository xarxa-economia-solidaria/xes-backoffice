package cat.xes.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cat.xes.model.Region;
import cat.xes.model.Town;

public interface TownsDao extends JpaRepository<Town, Long> {

	public List<Town> findByRegion(Region region);
}
