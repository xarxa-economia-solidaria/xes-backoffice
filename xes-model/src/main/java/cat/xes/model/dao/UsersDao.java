package cat.xes.model.dao;

import java.util.List;

import org.jamgo.model.jpa.entity.Role;

import cat.xes.model.XesUser;

public interface UsersDao extends org.jamgo.model.jpa.dao.UsersDao<XesUser> {

	@Override
	XesUser findByUsername(String username);

	List<XesUser> findByRoles(Role adminRole);

}
