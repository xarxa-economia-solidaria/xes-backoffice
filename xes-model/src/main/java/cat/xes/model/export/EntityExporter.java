package cat.xes.model.export;

import org.jamgo.model.jpa.portable.Importer;

import cat.xes.model.Entity;

public interface EntityExporter extends Importer<Entity> {

}
