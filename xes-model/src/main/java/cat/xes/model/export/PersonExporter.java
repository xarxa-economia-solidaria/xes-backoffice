package cat.xes.model.export;

import org.jamgo.model.jpa.portable.Importer;

import cat.xes.model.Person;

public interface PersonExporter extends Importer<Person> {

}
