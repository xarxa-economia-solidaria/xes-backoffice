//package cat.xes.model.literal;
//
//import javax.persistence.IdClass;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//import cat.xes.model.EntityState;
//
//@javax.persistence.Entity
//@Table(name = "entity_state_literal")
//@IdClass(ModelLiteralId.class)
//public class EntityStateLiteral extends Literal<EntityState> {
//
//	@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name = "id_model", referencedColumnName = "id", insertable = false, updatable = false)
//	private EntityState state;
//}
