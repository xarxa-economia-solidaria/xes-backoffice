package cat.xes.model.literal;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.jamgo.model.jpa.entity.Language;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cat.xes.model.LocalizedModel;

@IdClass(ModelLiteralId.class)
@MappedSuperclass
public class Literal<T extends LocalizedModel<? extends Literal<T>>> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Version
	@Column(name = "version")
	private Long version;

	@Id
	@Column(name = "id_model", insertable = false, updatable = false)
	private Long idModel;

	@Id
	@Column(name = "id_language", insertable = false, updatable = false)
	private Long idLanguage;

	@Column
	private String name;

	@ManyToOne
	@JoinColumn(name = "id_language", referencedColumnName = "id", insertable = true, updatable = false)
	protected Language language;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_model", referencedColumnName = "id", insertable = true, updatable = false)
	private T model;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
		if (language != null) {
			this.idLanguage = language.getId();
		}
	}

	public Long getIdModel() {
		return this.idModel;
	}

	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}

	public Long getIdLanguage() {
		return this.idLanguage;
	}

	public void setIdLanguage(Long idLanguage) {
		this.idLanguage = idLanguage;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public T getModel() {
		return this.model;
	}

	public void setModel(T model) {
		this.model = model;
		if (model != null) {
			this.idModel = model.getId();
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		if (this.idLanguage != null) {
			hash += this.idLanguage.hashCode();
		}
		if (this.idModel != null) {
			hash += this.idModel.hashCode();
		}
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Literal)) {
			return false;
		}
		Literal<?> literal = (Literal<?>) obj;
		return ((this.idLanguage == literal.getIdLanguage()) || ((this.idLanguage != null) && this.idLanguage.equals(literal.getIdLanguage())))
			&& ((this.idModel == literal.getIdModel()) || ((this.idModel != null) && this.idModel.equals(literal.getIdModel())));
	}

}
