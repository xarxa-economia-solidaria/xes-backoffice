package cat.xes.model.literal;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cat.xes.model.LocalizedModel;

@MappedSuperclass
public class LiteralExtended<T extends LocalizedModel<? extends Literal<T>>> extends Literal<T> {

	@JsonIgnore
	@Column
	private String description;

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
