package cat.xes.model.literal;

import java.io.Serializable;

public class ModelLiteralId implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long idModel;

	private Long idLanguage;

	public ModelLiteralId(Long idModel, Long idLanguage) {
		this.idModel = idModel;
		this.idLanguage = idLanguage;
	}

	@Override
	public int hashCode() {
//		if (this.getIdModel() != null) {
//			return (this.getIdModel().toString() + this.idLanguage.toString()).hashCode();
//		}
		if (this.getIdModel() != null) {
			int modelHash = this.getIdModel().toString().hashCode();
			if (this.idLanguage != null) {
				int languageHash = this.idLanguage.toString().hashCode();
				return modelHash + languageHash;
			}
			return modelHash;
		}
		return 0;
	}

	@Override
	public boolean equals(Object object) {
//		if (object instanceof ModelLiteralId) {
//			ModelLiteralId otherId = (ModelLiteralId) object;
//			return (otherId.getIdModel() == this.getIdModel()) && (otherId.idLanguage == this.idLanguage);
//		}
//		return false;

		if (!(object instanceof ModelLiteralId)) {
			return false;
		}
		ModelLiteralId literal = (ModelLiteralId) object;
		return ((this.idLanguage == literal.getIdLanguage()) || ((this.idLanguage != null) && this.idLanguage.equals(literal.getIdLanguage())))
			&& ((this.idModel == literal.getIdModel()) || ((this.idModel != null) && this.idModel.equals(literal.getIdModel())));
	}

	public Long getIdModel() {
		return this.idModel;
	}

	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}

	public Long getIdLanguage() {
		return this.idLanguage;
	}

	public void setIdLanguage(Long idLanguage) {
		this.idLanguage = idLanguage;
	}
}
