package cat.xes.model.dao.test;

import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import cat.xes.model.Entity;
import cat.xes.model.EntityState;
import cat.xes.model.EntityState.State;

public class EntityDaoTest extends ModelTest {

	@Test
	public void testGetEntity() {
		List<Entity> entities = this.entityDao.findAll();
		Assert.assertNotNull(entities);
		Assert.assertEquals(3, entities.size());

		Entity entity = entities.get(0);
		Assert.assertEquals("F65533259", entity.getNif());
		Assert.assertEquals("Jamgo", entity.getName());
		Assert.assertEquals("Cooperativa tecnologica", entity.getDescription());
		Assert.assertEquals("Doctor Trueta", entity.getAddress());
		Assert.assertEquals("info@jamgo.coop", entity.getEmail());

		Assert.assertNotNull(entity.getLegalForm());
		Assert.assertEquals(1L, entity.getLegalForm().getId().longValue());

		Assert.assertNotNull(entity.getSector());
		Assert.assertEquals(1L, entity.getSector().getId().longValue());

		Assert.assertEquals("555-666", entity.getPhone());
		Assert.assertNotNull(entity.getTown());
		Assert.assertEquals(1L, entity.getTown().getId().longValue());
		Assert.assertNotNull(entity.getUser());
		Assert.assertEquals("F65533259", entity.getUser().getUsername());
		Assert.assertEquals("jamgo.coop", entity.getWeb());
		Assert.assertEquals("2011", entity.getYear());
	}

	@Test
	public void testUpdateEntity() {
		Entity entity = this.entityDao.findByNif("F65533259");

		entity.setDescription("updated description");
		this.entityDao.save(entity);

		entity = this.entityDao.findByNif("F65533259");
		Assert.assertEquals("updated description", entity.getDescription());
	}

	@Test
	public void testEntityUpdate() {
		Entity entity = this.entityDao.findByNif("F65533259");
		Assert.assertNotNull(entity);

		Assert.assertEquals("F65533259", entity.getNif());

		entity.setName("Jamgo updated");
		entity.setDescription("updated description");
		entity.setAddress("Doctor Trueta updated");
		entity.setEmail("updated@jamgo.coop");
		entity.setLegalForm(this.legalFormsDao.getOne(2L));
		entity.setSector(this.sectorsDao.getOne(2L));
		entity.setPhone("555-777");
		entity.setTown(this.townsDao.getOne(2L));
		entity.setWeb("updated.jamgo.coop");
		entity.setYear("2012");

		this.entityDao.save(entity);
		entity = this.entityDao.findByNif("F65533259");
		Assert.assertNotNull(entity);
		Assert.assertEquals("F65533259", entity.getNif());
		Assert.assertEquals("updated description", entity.getDescription());
		Assert.assertEquals("Jamgo updated", entity.getName());
		Assert.assertEquals("Doctor Trueta updated", entity.getAddress());
		Assert.assertEquals("updated@jamgo.coop", entity.getEmail());

		Assert.assertNotNull(entity.getLegalForm());
		Assert.assertEquals(2L, entity.getLegalForm().getId().longValue());

		Assert.assertNotNull(entity.getSector());
		Assert.assertEquals(new Long(2L).longValue(), entity.getSector().getId().longValue());

		Assert.assertEquals("555-777", entity.getPhone());
		Assert.assertNotNull(entity.getTown());
		Assert.assertEquals(2L, entity.getTown().getId().longValue());
		Assert.assertEquals("updated.jamgo.coop", entity.getWeb());
		Assert.assertEquals("2012", entity.getYear());
	}

	@Test
	public void testStateChange() {
		EntityState registered = this.entityStateDao.findOne(State.Registered.getId());
		EntityState pending = this.entityStateDao.findOne(State.Pending.getId());

		Entity entity = this.entityDao.findByNif("A00000000");
		Assert.assertNotNull(entity);
		Assert.assertEquals(pending, entity.getXesState());

		entity.setXesState(registered);
		this.entityDao.save(entity);

		entity = this.entityDao.findByNif("A00000000");
		Assert.assertNotNull(entity);
		Assert.assertEquals(registered, entity.getXesState());
	}

	@Test
	public void testSearchByXes() {
		Set<Entity> entities = this.entityDao.findByXes(true);
		Assert.assertNotNull(entities);
		Assert.assertEquals(2, entities.size());
	}
}
