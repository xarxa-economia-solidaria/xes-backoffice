package cat.xes.model.dao.test;

import org.jamgo.model.jpa.dao.LanguageDao;
import org.jamgo.model.jpa.entity.Language;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import cat.xes.model.dao.EntitiesDao;
import cat.xes.model.dao.EntityStateDao;
import cat.xes.model.dao.LegalFormsDao;
import cat.xes.model.dao.RegionsDao;
import cat.xes.model.dao.SectorsDao;
import cat.xes.model.dao.TownsDao;
import cat.xes.model.dao.UsersDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-application-config.xml")
@Transactional
public abstract class ModelTest {

	@Autowired
	protected LanguageDao langDao;
	@Autowired
	protected EntitiesDao entityDao;
	@Autowired
	protected UsersDao userDao;
	@Autowired
	protected TownsDao townsDao;
	@Autowired
	protected LegalFormsDao legalFormsDao;
	@Autowired
	protected SectorsDao sectorsDao;
	@Autowired
	protected RegionsDao regionsDao;
	@Autowired
	protected UsersDao usersDao;
	@Autowired
	protected EntityStateDao entityStateDao;

	protected Language catalan;
	protected Language castellano;

	@Before
	public void setup() {
		this.catalan = this.langDao.findOne(1L);
		this.castellano = this.langDao.findOne(2L);
	}
}
