package cat.xes.model.dao.test;

import java.util.List;

import org.jamgo.model.jpa.entity.LocalizedString;
import org.junit.Assert;
import org.junit.Test;

import cat.xes.model.Sector;

public class SectorsDaoTest extends ModelTest {

	@Test
	public void testSectors() {
		List<Sector> sectors = this.sectorsDao.findAll();
		Assert.assertNotNull(sectors);
		Assert.assertEquals(3, sectors.size());

		Sector sector = this.sectorsDao.findOne(1L);
		Assert.assertNotNull(sector);
		Assert.assertEquals("sector-0", sector.getName().get(this.catalan.getLanguageCode()));
		Assert.assertEquals("sector-0", sector.getName().get(this.castellano.getLanguageCode()));
	}

	@Test
	public void testAddSector() {
		Sector sector = new Sector();
		LocalizedString sectorName = new LocalizedString();
		sectorName.set(this.catalan.getLanguageCode(), "Nou sector");
		sectorName.set(this.castellano.getLanguageCode(), "Nuevo sector");
		sector.setName(sectorName);
		sector = this.sectorsDao.saveAndFlush(sector);

		Sector retrievedSector = this.sectorsDao.getOne(sector.getId());
		Assert.assertEquals("Nuevo sector", retrievedSector.getName().get(this.castellano.getLanguageCode()));
		Assert.assertEquals("Nou sector", retrievedSector.getName().get(this.catalan.getLanguageCode()));
	}

	@Test
	public void testUpdateSectors() {
		Sector sector = this.sectorsDao.findOne(2L);
		Assert.assertNotNull(sector);

		sector.getName().set(this.catalan.getLanguageCode(), "updated-catala");
		sector.getName().set(this.castellano.getLanguageCode(), "updated-castellano");
		this.sectorsDao.save(sector);

		sector = this.sectorsDao.findOne(2L);
		Assert.assertEquals("updated-catala", sector.getName().get(this.catalan.getLanguageCode()));
		Assert.assertEquals("updated-castellano", sector.getName().get(this.castellano.getLanguageCode()));
	}

}
