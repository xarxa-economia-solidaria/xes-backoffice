package cat.xes.model.dao.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.jamgo.model.jpa.entity.Role;
import org.junit.Test;

import cat.xes.model.XesUser;

public class UserDaoTest extends ModelTest {

	@Test
	public void testUsers() {
		List<XesUser> usersList = this.usersDao.findAll();
		assertEquals(4, usersList.size());
		XesUser user = this.usersDao.findByUsername("user1");
		assertEquals("pass1", user.getPassword());
		assertEquals(1, user.getRoles().size());
		Role role = (Role) user.getRoles().toArray()[0];
		assertEquals("role_admin", role.getRolename());
	}
}
