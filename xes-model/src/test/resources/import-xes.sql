insert into user (id,username,password,enabled,version) values (1,'user1', 'pass1',1,0);
insert into user (id,username,password,enabled,version) values (2,'F65533259', 'pass1',1,0);
insert into user (id,username,password,enabled,version) values (3,'A00000000', 'pass2',1,0);
insert into user (id,username,password,enabled,version) values (4,'B11111111', 'pass3',1,0);

insert into role(id,rolename,version) values (1,'role_admin',0);
insert into role(id,rolename,version) values (2,'role_user',0);

insert into user_role(id_user,id_role) values (1, 1);
insert into user_role(id_user,id_role) values (2, 2);
insert into user_role(id_user,id_role) values (3, 2);
insert into user_role(id_user,id_role) values (4, 2);

insert into language(id,name, language_code, country_code, version) values (1,'català','ca','ES',0);
insert into language(id,name, language_code, country_code, version) values (2,'castellano','es','ES',0);

insert into sectors(id,version,name) values(1,0,'{texts:[{la:"ca",co:"ES",text:"sector-0"},{la:"es",co:"ES",text:"sector-0"}]}');
insert into sectors(id,version,name) values(2,0,'{texts:[{la:"ca",co:"ES",text:"sector-1"},{la:"es",co:"ES",text:"sector-1"}]}');
insert into sectors(id,version,name) values(3,0,'{texts:[{la:"ca",co:"ES",text:"sector-2"},{la:"es",co:"ES",text:"sector-2"}]}');

insert into provinces(id,version,name) values(1,0,'{texts:[{la:"ca",co:"ES",text:"Barcelona"},{la:"es",co:"ES",text:"Barcelona"}]}');
insert into provinces(id,version,name) values(2,0,'{texts:[{la:"ca",co:"ES",text:"Tarragona"},{la:"es",co:"ES",text:"Tarragona"}]}');

insert into regions(id,version,name) values(1,0,'{texts:[{la:"ca",co:"ES",text:"Girona"},{la:"es",co:"ES",text:"Gerona"}]}');
insert into regions(id,version,name) values(2,0,'{texts:[{la:"ca",co:"ES",text:"Barcelona"},{la:"es",co:"ES",text:"Barcelona"}]}');

insert into towns(id,id_region,version,name) values (1,1,0,'{texts:[{la:"ca",co:"ES",text:"Platja d''aro"},{la:"es",co:"ES",text:"Playa de aro"}]}');
insert into towns(id,id_region,version,name) values (2,2,0,'{texts:[{la:"ca",co:"ES",text:"Terrassa"},{la:"es",co:"ES",text:"Tarrassa"}]}');
insert into towns(id,id_region,version,name) values (3,2,0,'{texts:[{la:"ca",co:"ES",text:"L''Hospitalet"},{la:"es",co:"ES",text:"Hospitalet"}]}');

insert into legal_forms(id,version,name) values(1,0,'{texts:[{la:"ca",co:"ES",text:"Cooperativa"},{la:"es",co:"ES",text:"Cooperativa"}]}');
insert into legal_forms(id,version,name) values(2,0,'{texts:[{la:"ca",co:"ES",text:"Cooperativa de 2n grau"},{la:"es",co:"ES",text:"Cooperativa de 2do grado"}]}');

insert into entity_state(id,version,name) values(1,0,'{texts:[{la:"ca",co:"ES",text:"Pendent"},{la:"es",co:"ES",text:"Pendiente"}]}');
insert into entity_state(id,version,name) values(2,0,'{texts:[{la:"ca",co:"ES",text:"Registrada"},{la:"es",co:"ES",text:"Registrada"}]}');
insert into entity_state(id,version,name) values(3,0,'{texts:[{la:"ca",co:"ES",text:"Declinada"},{la:"es",co:"ES",text:"Declinada"}]}');

insert into evaluable_type(id,version,name) values (1,0,'Entity');

insert into evaluable(id,version,id_evaluable_type) values (2,0,1);
insert into entities(id,nif,name,description,web,creationyear,address,phone,email,id_town,id_legal_form,id_sector,id_xes_state,id_user,is_xes)
values (2,'F65533259','Jamgo','Cooperativa tecnologica','jamgo.coop',2011,'Doctor Trueta','555-666','info@jamgo.coop',1,'1',1,2,2, true);

insert into evaluable(id,version,id_evaluable_type) values (3,0,1);
insert into entities(id,nif,name,id_legal_form,id_sector,phone,email,web,creationyear,id_xes_state,id_user,is_xes) 
values (3,'A00000000','Test1',1,1,'test1-phone','test1@test.org','test1.org',2011,1,3, true);

insert into evaluable(id,version,id_evaluable_type) values (4,0,1);
insert into entities(id,nif,name,id_legal_form,id_sector,phone,email,web,creationyear,id_xes_state,id_user) 
values (4,'B11111111','Test2',1,1,'test2-phone','test2@test.org','test2.org',2010,2,4);