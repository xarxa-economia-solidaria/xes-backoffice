package cat.xes.service;

import java.util.List;
import java.util.Set;

import cat.xes.model.Entity;
import cat.xes.model.Person;
import cat.xes.service.data.PersonData;
import cat.xes.service.exception.PersonAlreadyExistsException;

public interface RegisterServces {

	public void requestReqistration(Entity entity);

	public void acceptEntityRegistrationRequest(Long id);

	public void declineEntityRegistrationRequest(Long id);

	public Set<Entity> getPendingRegistrationEntities();

	public void requestReqistration(PersonData personData) throws PersonAlreadyExistsException;

	public void acceptPersonRegistrationRequest(Long id);

	public void declinePersonRegistrationRequest(Long id);

	public List<Person> getPendingRegistrationPersons();
}
