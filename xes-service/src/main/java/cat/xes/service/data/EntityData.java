package cat.xes.service.data;

import java.util.Date;

import org.jamgo.model.jpa.entity.Language;
import org.jamgo.model.jpa.entity.LocalizedString;

import cat.xes.model.Entity;

public class EntityData {

	private long id;
	private String nif;
	private String name;
	private String shortName;
	private String description;
	private String web;
	private String address;
	private String cp;
	private String phone;
	private String email;
	private Long townId;
	private String otherTown;
	private Long regionId;
	private Long provinceId;
	private Long legalFormId;
	private Long parentLegalFormId;
	private Long sectorId;
	private Long parentSectorId;
	private String pamAPamUrl;
	private boolean hasLogo;
	private long lastQuestionaryId = -1;
	private Date registryDate;
	private boolean allowInfo;
	private int partnerNumber;
	private byte[] logo;
	private LocalizedString xesDescription;
	private String year;

	private String contactName;
	private String contactSurname;
	private String contactEmail;
	private String contactPhone;
	private Language language;
	private String bankAccountOwner;
	private String bankAccountIban;
	private String administrationContact;

	public EntityData() {

	}

	public EntityData(Entity entity) {
		this.id = entity.getId();
		this.nif = entity.getNif();
		this.name = entity.getName();
		this.shortName = entity.getShortName();
		this.description = entity.getDescription();
		this.web = entity.getWeb();
		this.address = entity.getAddress();
		this.cp = entity.getCp();
//		this.phone = entity.getPhone();
//		this.email = entity.getEmail();
		if (entity.getTown() != null) {
			this.townId = entity.getTown().getId();
		}
		if (entity.getRegion() != null) {
			this.regionId = entity.getRegion().getId();
		}
		if (entity.getProvince() != null) {
			this.provinceId = entity.getProvince().getId();
		}
		if (entity.getLegalForm() != null) {
			this.legalFormId = entity.getLegalForm().getId();
			if (entity.getLegalForm().getParent() != null) {
				this.parentLegalFormId = entity.getLegalForm().getParent().getId();
			}
		}
		if (entity.getSector() != null) {
			this.sectorId = entity.getSector().getId();
			if (entity.getSector().getParent() != null) {
				this.parentSectorId = entity.getSector().getParent().getId();
			}
		}
		this.pamAPamUrl = entity.getPamAPamUrl();
		this.setHasLogo((entity.getLogo() != null) && (entity.getLogo().length > 0));
	}

	public String getNif() {
		return this.nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCp() {
		return this.cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOtherTown() {
		return this.otherTown;
	}

	public void setOtherTown(String otherTown) {
		this.otherTown = otherTown;
	}

	public Long getTownId() {
		return this.townId;
	}

	public void setTownId(Long townId) {
		this.townId = townId;
	}

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public Long getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public Long getLegalFormId() {
		return this.legalFormId;
	}

	public void setLegalFormId(Long legalFormId) {
		this.legalFormId = legalFormId;
	}

	public Long getParentLegalFormId() {
		return this.parentLegalFormId;
	}

	public void setParentLegalFormId(Long parentLegalFormId) {
		this.parentLegalFormId = parentLegalFormId;
	}

	public Long getSectorId() {
		return this.sectorId;
	}

	public void setSectorId(Long sectorId) {
		this.sectorId = sectorId;
	}

	public Long getParentSectorId() {
		return this.parentSectorId;
	}

	public void setParentSectorId(Long parentSectorId) {
		this.parentSectorId = parentSectorId;
	}

	public String getPamAPamUrl() {
		return this.pamAPamUrl;
	}

	public void setPamAPamUrl(String pamAPamUrl) {
		this.pamAPamUrl = pamAPamUrl;
	}

	public long getLastQuestionaryId() {
		return this.lastQuestionaryId;
	}

	public void setLastQuestionaryId(long lastQuestionaryId) {
		this.lastQuestionaryId = lastQuestionaryId;
	}

	public boolean isHasLogo() {
		return this.hasLogo;
	}

	public void setHasLogo(boolean hasLogo) {
		this.hasLogo = hasLogo;
	}

	public Date getRegistryDate() {
		return this.registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public boolean isAllowInfo() {
		return this.allowInfo;
	}

	public void setAllowInfo(boolean allowInfo) {
		this.allowInfo = allowInfo;
	}

	public int getPartnerNumber() {
		return this.partnerNumber;
	}

	public void setPartnerNumber(int partnerNumber) {
		this.partnerNumber = partnerNumber;
	}

	public byte[] getLogo() {
		return this.logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public LocalizedString getXesDescription() {
		return this.xesDescription;
	}

	public void setXesDescription(LocalizedString xesDescription) {
		this.xesDescription = xesDescription;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactSurname() {
		return this.contactSurname;
	}

	public void setContactSurname(String contactSurname) {
		this.contactSurname = contactSurname;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactPhone() {
		return this.contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getBankAccountOwner() {
		return this.bankAccountOwner;
	}

	public void setBankAccountOwner(String bankAccountOwner) {
		this.bankAccountOwner = bankAccountOwner;
	}

	public String getBankAccountIban() {
		return this.bankAccountIban;
	}

	public void setBankAccountIban(String bankAccountIban) {
		this.bankAccountIban = bankAccountIban;
	}

	public String getAdministrationContact() {
		return this.administrationContact;
	}

	public void setAdministrationContact(String administrationContact) {
		this.administrationContact = administrationContact;
	}
}
