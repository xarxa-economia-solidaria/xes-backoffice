package cat.xes.service.exception;

public class PersonAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 1L;

	public PersonAlreadyExistsException(String message) {
		super(message);
	}

}
