package cat.xes.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jamgo.model.jpa.entity.LocalizedString;
import org.jamgo.model.jpa.entity.Model;
import org.jamgo.model.jpa.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.exception.NoClassParentRelationDefinedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cat.xes.model.LegalForm;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;
import cat.xes.model.dao.JpaRepositoryFactory;
import cat.xes.model.dao.LegalFormsDao;
import cat.xes.model.dao.RegionsDao;
import cat.xes.model.dao.SectorsDao;
import cat.xes.model.dao.TownsDao;

@Service
public class DatasourceServicesImpl extends org.jamgo.services.impl.DatasourceServicesImpl {

	@Autowired
	private TownsDao townsDao;
	@Autowired
	private RegionsDao regionsDao;
	@Autowired
	private SectorsDao sectorsDao;
	@Autowired
	private LegalFormsDao legalFormsDao;

	@Autowired
	private JpaRepositoryFactory repositoryFactory;

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Model> List<T> getAll(Class<T> clazz) throws RepositoryForClassNotDefinedException {
		List<T> items = (List<T>) this.repositoryFactory.getRepository(clazz).findAll();
		if (!items.isEmpty()) {
			Collections.sort(items, new Comparator<Model>() {
				@Override
				public int compare(Model model1, Model model2) {
					try {
						Method method = model1.getClass().getMethod("getName");
						if ((method != null) && (method.getReturnType().getClass().equals(String.class))) {
							String name1 = (String) model1.getClass().getMethod("getName").invoke(model1);
							String name2 = (String) model2.getClass().getMethod("getName").invoke(model2);
							return name1.compareTo(name2);
						} else if ((method != null) && (method.getReturnType().getClass().equals(LocalizedString.class))) {
							LocalizedString name1 = (LocalizedString) model1.getClass().getMethod("getName").invoke(model1);
							LocalizedString name2 = (LocalizedString) model2.getClass().getMethod("getName").invoke(model2);
							return name1.compareTo(name2);
						}
						return 0;
					} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						return 0;
					}
				}
			});
		}
		return items;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Model> List<T> getAll(Class<T> clazz, Object parent) throws NoClassParentRelationDefinedException {
		if (clazz.equals(LegalForm.class)) {
			if (parent != null) {
				return (List<T>) this.legalFormsDao.findByParent((LegalForm) parent);
			}
			return (List<T>) this.legalFormsDao.findByParent(null);
		} else if (clazz.equals(Sector.class)) {
			if (parent != null) {
				return (List<T>) this.sectorsDao.findByParent((Sector) parent);
			} else {
				return (List<T>) this.sectorsDao.findByParent(null);
			}
		} else if (parent != null) {
			if (clazz.equals(Town.class) && (parent instanceof Region)) {
				return (List<T>) this.townsDao.findByRegion((Region) parent);
			} else if (clazz.equals(Region.class)) {
				return (List<T>) this.regionsDao.findByProvince((Province) parent);
			}
		} else {
			return new ArrayList<T>();
		}
		throw new NoClassParentRelationDefinedException(clazz, parent.getClass());
	}
}
