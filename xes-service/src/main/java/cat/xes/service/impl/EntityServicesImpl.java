package cat.xes.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.jamgo.model.jpa.dao.LanguageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cat.xes.model.Entity;
import cat.xes.model.EntityState;
import cat.xes.model.EntityState.State;
import cat.xes.model.EvaluableType;
import cat.xes.model.EvaluableType.Type;
import cat.xes.model.XesUser;
import cat.xes.model.dao.EntitiesDao;
import cat.xes.model.dao.EntityStateDao;
import cat.xes.model.dao.EvaluableTypeDao;
import cat.xes.model.dao.LegalFormsDao;
import cat.xes.model.dao.SectorsDao;
import cat.xes.model.dao.TownsDao;
import cat.xes.service.EntityServices;
import cat.xes.service.data.EntityData;
import cat.xes.service.exception.EntityAlreadyExistsException;
import cat.xes.service.search.EntitySearch;

@Service
public class EntityServicesImpl implements EntityServices {

	@Autowired
	private EntitiesDao entitiesDao;
	@Autowired
	private EntityStateDao entityStateDao;
	@Autowired
	private EntityStateDao stateDao;
	@Autowired
	private TownsDao townsDao;
	@Autowired
	private LegalFormsDao legalFormsDao;
	@Autowired
	private SectorsDao sectorsDao;
	@Autowired
	private LanguageDao languagesDao;
	@Autowired
	private EvaluableTypeDao evaluableTypeDao;

	@Override
	public Entity get(String nif) {
		return this.entitiesDao.findByNif(nif);
	}

	@Override
	public Set<Entity> search(EntitySearch entitySearch) {
		return this.entitiesDao.findBySectorAndXesStateAndLegalForm(entitySearch.getSector(), entitySearch.getState(), entitySearch.getLegalForm());
	}

	@Override
	public List<Entity> getAll() {
		return this.entitiesDao.findAll();
	}

	@Override
	public List<EntityData> getAllEntitiesData() {
		List<EntityData> entitiesData = new ArrayList<EntityData>();
		Set<Entity> entities = this.entitiesDao.findByXes(true);
		for (Entity entity : entities) {
			if (entity.isXesActive() && (entity.getXesState() != null) && (entity.getXesState().getId().equals(State.Registered.getId()))) {
				EntityData entityData = new EntityData(entity);
				entitiesData.add(entityData);
			}
		}

		return entitiesData;

	}

	@Override
	public Entity update(Entity entity) {
		return this.entitiesDao.save(entity);
	}

	@Override
	public EntityState getState(State state) {
		return this.entityStateDao.findOne(state.getId());
	}

	@Override
	public byte[] getLogo(String nif) {
		return this.entitiesDao.getLogo(nif);
	}

	@Override
	public void deleteEntity(Entity entity) {
		this.entitiesDao.delete(entity);
	}

	@Override
	public void register(EntityData entityData) throws EntityAlreadyExistsException {
		Entity entity = this.entitiesDao.findByNif(entityData.getNif());
		if (Optional.ofNullable(entity).map(Entity::isXes).orElse(false)) {
			throw new EntityAlreadyExistsException(entityData.getNif());
		}
		entity = this.setEntityInfoFromData(entity, entityData);
		entity.setType(this.getEvaluableType(EvaluableType.Type.Entity));
		entity.setXes(true);
		// set pending state
		entity.setXesState(this.stateDao.getOne(EntityState.State.Pending.getId()));
		entity.setXesRegistryRequestDate(new Date());
		this.entitiesDao.save(entity);
	}

	public boolean exists(String nif) {
		return this.entitiesDao.findByNif(nif) != null;
	}

	private Entity setEntityInfoFromData(Entity entity, EntityData entityData) {
		if (entity == null) {
			entity = new Entity();
		}
		entity.setName(entityData.getName());
		entity.setNif(entityData.getNif());
		entity.setAddress(entityData.getAddress());
		entity.setAllowXesInfo(entityData.isAllowInfo());
		entity.setCp(entityData.getCp());
		entity.setDescription(entityData.getDescription());
		entity.setEmail(entityData.getEmail());
		entity.setLogo(entityData.getLogo());
		entity.setOtherTown(entityData.getOtherTown());
		entity.setPamAPamUrl(entityData.getPamAPamUrl());
		entity.setPartnerNumber(entityData.getPartnerNumber());
		entity.setPhone(entityData.getPhone());
		entity.setShortName(entityData.getShortName());
		entity.setWeb(entityData.getWeb());
		entity.setXesDescription(entityData.getXesDescription());
		entity.setYear(entityData.getYear());
		entity.setBankAccountIban(entityData.getBankAccountIban());
		entity.setBankAccountOwner(entityData.getBankAccountOwner());
		entity.setAdministrationContact(entityData.getAdministrationContact());
		if (entityData.getTownId() != null) {
			entity.setTown(this.townsDao.findOne(entityData.getTownId()));
		}
		if (entityData.getLegalFormId() != null) {
			entity.setLegalForm(this.legalFormsDao.findOne(entityData.getLegalFormId()));
		}
		if (entityData.getSectorId() != null) {
			entity.setSector(this.sectorsDao.findOne(entityData.getSectorId()));
		}
		entity.setXesRegistryRequestDate(entityData.getRegistryDate());

		// set user
		XesUser user = entity.getUser();
		if (entity.getUser() == null) {
			user = new XesUser();
		}
		user.setEntity(entity);
		user.setUsername(entity.getNif());
		user.setEmail(entityData.getContactEmail());
		user.setName(entityData.getContactName());
		user.setPhone(entityData.getContactPhone());
		user.setSurname(entityData.getContactSurname());
		if (entityData.getLanguage() != null) {
			user.setLanguage(this.languagesDao.findOne(entityData.getLanguage().getId()));
		}
		entity.setUser(user);

		return entity;
	}

	@Override
	public EvaluableType getEvaluableType(Type type) {
		return this.evaluableTypeDao.findByType(type);
	}
	
	
}
