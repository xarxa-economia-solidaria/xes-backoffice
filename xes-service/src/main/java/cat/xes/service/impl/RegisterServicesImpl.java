package cat.xes.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.jamgo.model.jpa.dao.LanguageDao;
import org.jamgo.model.jpa.dao.RolesDao;
import org.jamgo.model.jpa.entity.Language;
import org.jamgo.model.jpa.entity.Role;
import org.jamgo.model.jpa.util.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cat.xes.model.Entity;
import cat.xes.model.EntityState;
import cat.xes.model.Person;
import cat.xes.model.Town;
import cat.xes.model.XesUser;
import cat.xes.model.dao.EntitiesDao;
import cat.xes.model.dao.EntityStateDao;
import cat.xes.model.dao.PersonDao;
import cat.xes.service.RegisterServces;
import cat.xes.service.data.PersonData;
import cat.xes.service.exception.PersonAlreadyExistsException;

@Service
public class RegisterServicesImpl implements RegisterServces {

	@Autowired
	private EntitiesDao entitiesDao;
	@Autowired
	private EntityStateDao entityStateDao;
	@Autowired
	private RolesDao rolesDao;
	@Autowired
	private PersonDao personDao;
	@Autowired
	private LanguageDao languagesDao;
	@Autowired
	private EntityStateDao stateDao;

	@Value("${mail.from.register}")
	private String mailFrom;

	@Override
	public void requestReqistration(Entity entity) {
		EntityState pendingState = this.entityStateDao.getOne(EntityState.State.Pending.getId());
		entity.setXesState(pendingState);
		entity.setXesRegistryRequestDate(new Date());
		entity.getUser().setUsername(entity.getNif());

		this.entitiesDao.save(entity);

		// TODO definir mail de la plataforma
//		this.mailService.sendRegistrationMail(entity);
	}

	@Override
	public void acceptEntityRegistrationRequest(Long entityId) {
		Entity entity = this.entitiesDao.getOne(entityId);
		EntityState registeredState = this.entityStateDao.getOne(EntityState.State.Registered.getId());
		entity.setXesState(registeredState);
		entity.setActive(true);
		entity.setXesRegistryResolutionDate(new Date());
		String password = PasswordUtils.generatePassword();
		XesUser user = entity.getUser();
		if (user.getPassword() == null) {
			user.setPassword(PasswordUtils.encodePassword(password));
		}
		user.setEnabled(true);
		if (user.getLanguage() == null) {
			user.setLanguage(this.languagesDao.findOne(Language.CATALAN));
		}
		Role userRole = this.rolesDao.findByRolename(Role.ROLE_USER);
		if (!user.getRoles().contains(userRole)) {
			user.getRoles().add(userRole);
		}
		this.entitiesDao.save(entity);

		// TODO send acceptance mail
//		this.mailService.sendAcceptanceMail(entity, password);
	}

	@Override
	public void declineEntityRegistrationRequest(Long entityId) {
		Entity entity = this.entitiesDao.getOne(entityId);
		EntityState registeredState = this.entityStateDao.getOne(EntityState.State.Declined.getId());
		entity.setXesState(registeredState);
		entity.setXesRegistryResolutionDate(new Date());
		this.entitiesDao.save(entity);

//		this.mailService.sendDeclinationMail(entity);
	}

	@Override
	public Set<Entity> getPendingRegistrationEntities() {
		EntityState pendingState = this.entityStateDao.getOne(EntityState.State.Pending.getId());
		return this.entitiesDao.findByXesState(pendingState);
	}

	@Override
	public void requestReqistration(PersonData personData) throws PersonAlreadyExistsException {
		Person person = this.personDao.findByNif(personData.getNif());
		EntityState declinedState = this.stateDao.getOne(EntityState.State.Declined.getId());
		if ((person != null) && !person.getState().equals(declinedState)) {
			throw new PersonAlreadyExistsException(personData.getNif());
		}
		person = this.setEntityInfoFromData(person, personData);
		// set pending state
		person.setState(this.stateDao.getOne(EntityState.State.Pending.getId()));
		person.setRegistryRequestDate(new Date());
		this.personDao.save(person);
		// TODO definir mail de la plataforma
//		this.mailService.sendRegistrationMail(entity);
	}

	@Override
	public void acceptPersonRegistrationRequest(Long personId) {
		Person person = this.personDao.getOne(personId);
		EntityState registeredState = this.entityStateDao.getOne(EntityState.State.Registered.getId());
		person.setState(registeredState);
		person.setRegistryResolutionDate(new Date());
		this.personDao.save(person);

		// TODO send acceptance mail
//		this.mailService.sendAcceptanceMail(entity, password);
	}

	@Override
	public void declinePersonRegistrationRequest(Long personId) {
		Person person = this.personDao.getOne(personId);
		EntityState registeredState = this.entityStateDao.getOne(EntityState.State.Declined.getId());
		person.setState(registeredState);
		person.setRegistryResolutionDate(new Date());
		this.personDao.save(person);

//		this.mailService.sendDeclinationMail(entity);
	}

	@Override
	public List<Person> getPendingRegistrationPersons() {
		EntityState pendingState = this.entityStateDao.getOne(EntityState.State.Pending.getId());
		return this.personDao.findByState(pendingState);
	}

	private Person setEntityInfoFromData(Person person, PersonData personData) {
		if (person == null) {
			person = new Person();
		}
		person.setId(person.getId());
		person.setVersion(person.getVersion());
		person.setName(personData.getName());
		person.setSurnames(personData.getSurnames());
		person.setNif(personData.getNif());
		person.setAddress(personData.getAddress());
		person.setCp(personData.getCp());
		person.setEmail(personData.getEmail());
		Town town = new Town();
		town.setId(personData.getTownId());
		person.setTown(town);
		person.setOtherTown(personData.getOtherTown());
		person.setPhone(personData.getPhone());
		person.setBankAccountIban(personData.getBankAccountIban());
		person.setBankAccountOwner(personData.getBankAccountOwner());
		person.setRegistryRequestDate(new Date());
		person.setQuote(personData.getQuote());
		person.setVoluntaryQuote(personData.getVoluntaryQuote());

		return person;
	}

}
