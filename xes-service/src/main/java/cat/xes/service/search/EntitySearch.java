package cat.xes.service.search;

import cat.xes.model.EntityState;
import cat.xes.model.LegalForm;
import cat.xes.model.Sector;

public class EntitySearch {

	private EntityState state;
	private LegalForm legalForm;
	private Sector sector;

	public EntityState getState() {
		return this.state;
	}

	public void setState(EntityState state) {
		this.state = state;
	}

	public LegalForm getLegalForm() {
		return this.legalForm;
	}

	public void setLegalForm(LegalForm legalForm) {
		this.legalForm = legalForm;
	}

	public Sector getSector() {
		return this.sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

}
