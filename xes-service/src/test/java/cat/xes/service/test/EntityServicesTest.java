package cat.xes.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cat.xes.model.Entity;
import cat.xes.model.EntityState.State;
import cat.xes.service.EntityServices;
import cat.xes.service.data.EntityData;
import cat.xes.service.exception.EntityAlreadyExistsException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
public class EntityServicesTest {

	@Autowired
	private EntityServices entityServices;

	@Test
	public void testRegistryNotExistingEntity() {
		try {
			EntityData entityData = new EntityData();
			entityData.setNif("F33333333");
			entityData.setName("Test");
			entityData.setPhone("123456");
			entityData.setEmail("mail@mail.com");
			this.entityServices.register(entityData);

			Entity entity = this.entityServices.get("F22222222");
			Assert.assertEquals(State.Pending.getId(), entity.getXesState().getId());
		} catch (EntityAlreadyExistsException e) {
			Assert.fail();
		}
	}

	@Test
	public void testRegistryExistingXesEntity() {
		try {
			EntityData entityData = new EntityData();
			entityData.setNif("F11111111");
			this.entityServices.register(entityData);
			Assert.fail();
		} catch (EntityAlreadyExistsException e) {
		}
	}

	@Test
	public void testRegistryExistingNotXesEntity() {
		try {
			EntityData entityData = new EntityData();
			entityData.setNif("F22222222");
			entityData.setPhone("123456");
			entityData.setEmail("mail@mail.com");
			this.entityServices.register(entityData);

			Entity entity = this.entityServices.get("F22222222");
			Assert.assertEquals(State.Pending.getId(), entity.getXesState().getId());
		} catch (EntityAlreadyExistsException e) {
			Assert.fail();
		}
	}
}
