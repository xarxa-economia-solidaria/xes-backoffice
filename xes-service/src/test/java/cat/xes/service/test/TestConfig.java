package cat.xes.service.test;

import org.jamgo.model.jpa.PackageMarker;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import cat.xes.model.ModelConfiguration;
import cat.xes.service.ServiceConfiguration;

@Configuration
@Import({ ModelConfiguration.class, ServiceConfiguration.class })
@ComponentScan(basePackageClasses = { PackageMarker.class }) // Instead of @SpringBootApplication, this is necessary to scan components from other jars
@EntityScan(basePackageClasses = { PackageMarker.class, ModelConfiguration.class })
@EnableJpaRepositories(basePackageClasses = { PackageMarker.class, ModelConfiguration.class })
@PropertySource("classpath:application.properties")
public class TestConfig {

}
