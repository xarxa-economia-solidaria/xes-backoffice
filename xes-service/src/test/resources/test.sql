insert into entity_state(id,version,name) values(1,0,'{texts:[{la:"ca",co:"ES",text:"Pendent"},{la:"es",co:"ES",text:"Pendiente"}]}');
insert into entity_state(id,version,name) values(2,0,'{texts:[{la:"ca",co:"ES",text:"Registrada"},{la:"es",co:"ES",text:"Registrada"}]}');
insert into entity_state(id,version,name) values(3,0,'{texts:[{la:"ca",co:"ES",text:"Declinada"},{la:"es",co:"ES",text:"Declinada"}]}');
insert into entity_state(id,version,name) values(4,0,'{texts:[{la:"ca",co:"ES",text:"Completada"},{la:"es",co:"ES",text:"Completada"}]}');

insert into evaluable_type(id,version,name) values (1,0,'Entity');

insert into evaluable(id,version,id_evaluable_type) values (1,0,1);
insert into entities(id,nif,name,email,phone,id_xes_state,is_xes) values (1,'F11111111','Entity1','entity1@mail.com','123456',1,true);

insert into evaluable(id,version,id_evaluable_type) values (2,0,1);
insert into entities(id,nif,name,email,phone,id_xes_state) values (2,'F22222222','Entity2','entity2@mail.com','123456',1);

