package cat.xes.service;

import java.util.List;

import cat.xes.model.Model;
import cat.xes.model.dao.JpaRepositoryFactory.RepositoryForClassNotDefinedException;
import cat.xes.service.impl.DatasourceServicesImpl.NoClassParentRelationDefined;

public interface DatasourceServices {

	public <T extends Model> List<T> getAll(Class<T> clazz) throws RepositoryForClassNotDefinedException;

	public <T extends Model> List<T> getAll(Class<T> clazz, Object parent) throws NoClassParentRelationDefined;
}
