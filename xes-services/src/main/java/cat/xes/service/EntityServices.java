package cat.xes.service;

import java.util.List;
import java.util.Set;

import cat.xes.model.Entity;
import cat.xes.model.EntityState;
import cat.xes.model.EntityState.State;
import cat.xes.service.data.EntityData;
import cat.xes.service.search.EntitySearch;

public interface EntityServices {

	public Entity get(String nif);

	public Entity update(Entity entity);

	public List<Entity> getAll();

	public Set<Entity> search(EntitySearch entitySearch);

	public EntityState getState(State state);

	public List<EntityData> getAllEntitiesData();

	public byte[] getLogo(String nif);

	public void deleteEntity(Entity bean);

}
