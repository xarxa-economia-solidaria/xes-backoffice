package cat.xes.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import cat.xes.model.config.ModelConfiguration;

@Configuration
@Import(ModelConfiguration.class)
@ComponentScan(basePackageClasses = { ServiceConfiguration.class })
public class ServiceConfiguration {

}
