package cat.xes.service.data;

import java.util.Set;

import cat.xes.model.Association;
import cat.xes.model.Entity;
import cat.xes.model.LegalForm;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;

public class EntityData {

	private String nif;
	private String name;
	private String shortName;
	private String description;
	private String web;
	private String address;
	private String cp;
	private String phone;
	private String email;
	private Town town;
	private String otherTown;
	private Region region;
	private Province province;
	private LegalForm legalForm;
	private Sector sector;
	private Set<Association> associations;
	private String pamAPamUrl;
	private boolean hasLogo;
	private long lastQuestionaryId = -1;

	public EntityData() {

	}

	public EntityData(Entity entity) {
		this.nif = entity.getNif();
		this.name = entity.getName();
		this.shortName = entity.getShortName();
		this.description = entity.getDescription();
		this.web = entity.getWeb();
		this.address = entity.getAddress();
		this.cp = entity.getCp();
		this.phone = entity.getPhone();
		this.email = entity.getEmail();
		this.town = entity.getTown();
		this.region = entity.getRegion();
		this.province = entity.getProvince();
		this.legalForm = entity.getLegalForm();
		this.sector = entity.getSector();
		this.associations = entity.getAssociations();
		this.pamAPamUrl = entity.getPamAPamUrl();
		this.setHasLogo((entity.getLogo() != null) && (entity.getLogo().length > 0));
	}

	public String getNif() {
		return this.nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCp() {
		return this.cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Town getTown() {
		return this.town;
	}

	public void setTown(Town town) {
		this.town = town;
	}

	public String getOtherTown() {
		return this.otherTown;
	}

	public void setOtherTown(String otherTown) {
		this.otherTown = otherTown;
	}

	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public LegalForm getLegalForm() {
		return this.legalForm;
	}

	public void setLegalForm(LegalForm legalForm) {
		this.legalForm = legalForm;
	}

	public Sector getSector() {
		return this.sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public Set<Association> getAssociations() {
		return this.associations;
	}

	public void setAssociations(Set<Association> associations) {
		this.associations = associations;
	}

	public String getPamAPamUrl() {
		return this.pamAPamUrl;
	}

	public void setPamAPamUrl(String pamAPamUrl) {
		this.pamAPamUrl = pamAPamUrl;
	}

	public long getLastQuestionaryId() {
		return this.lastQuestionaryId;
	}

	public void setLastQuestionaryId(long lastQuestionaryId) {
		this.lastQuestionaryId = lastQuestionaryId;
	}

	public boolean isHasLogo() {
		return this.hasLogo;
	}

	public void setHasLogo(boolean hasLogo) {
		this.hasLogo = hasLogo;
	}
}
