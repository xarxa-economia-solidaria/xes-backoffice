package cat.xes.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cat.xes.model.LegalForm;
import cat.xes.model.Model;
import cat.xes.model.Province;
import cat.xes.model.Region;
import cat.xes.model.Sector;
import cat.xes.model.Town;
import cat.xes.model.dao.JpaRepositoryFactory;
import cat.xes.model.dao.JpaRepositoryFactory.RepositoryForClassNotDefinedException;
import cat.xes.model.dao.LegalFormsDao;
import cat.xes.model.dao.RegionsDao;
import cat.xes.model.dao.SectorsDao;
import cat.xes.model.dao.TownsDao;
import cat.xes.service.DatasourceServices;

@Service
public class DatasourceServicesImpl implements DatasourceServices {

	@Autowired
	private TownsDao townsDao;
	@Autowired
	private RegionsDao regionsDao;
	@Autowired
	private SectorsDao sectorsDao;
	@Autowired
	private LegalFormsDao legalFormsDao;

	@Autowired
	private JpaRepositoryFactory repositoryFactory;

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Model> List<T> getAll(Class<T> clazz) throws RepositoryForClassNotDefinedException {
		List<T> items = (List<T>) this.repositoryFactory.getRepository(clazz).findAll();
		if (!items.isEmpty()) {
			Collections.sort(items, new Comparator<Model>() {
				@Override
				public int compare(Model model1, Model model2) {
					try {
						if (model1.getClass().getMethod("getName") != null) {
							String name1 = (String) model1.getClass().getMethod("getName").invoke(model1);
							String name2 = (String) model2.getClass().getMethod("getName").invoke(model2);
							return name1.compareTo(name2);
						}
						return 0;
					} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						return 0;
					}
				}
			});
		}
//		return new LinkedHashSet<T>(items);
//		return new HashSet<T>(items);
		return items;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Model> List<T> getAll(Class<T> clazz, Object parent) throws NoClassParentRelationDefined {
		if (clazz.equals(LegalForm.class)) {
			if (parent != null) {
				return (List<T>) this.legalFormsDao.findByParent((LegalForm) parent);
			}
			return (List<T>) this.legalFormsDao.findByParent(null);
		} else if (clazz.equals(Sector.class)) {
			if (parent != null) {
				return (List<T>) this.sectorsDao.findByParent((Sector) parent);
			} else {
				return (List<T>) this.sectorsDao.findByParent(null);
			}
		} else if (parent != null) {
			if (clazz.equals(Town.class) && (parent instanceof Region)) {
				return (List<T>) this.townsDao.findByRegion((Region) parent);
			} else if (clazz.equals(Region.class)) {
				return (List<T>) this.regionsDao.findByProvince((Province) parent);
			}
		} else {
			return new ArrayList<T>();
		}
		throw new NoClassParentRelationDefined(clazz, parent.getClass());
	}

	public class NoClassParentRelationDefined extends Exception {

		private static final long serialVersionUID = 1L;

		public NoClassParentRelationDefined(Class<?> clazz, Class<?> parent) {
			super("Can't find items for class " + clazz.getName() + " with parent " + parent.getName());
		}
	}
}
