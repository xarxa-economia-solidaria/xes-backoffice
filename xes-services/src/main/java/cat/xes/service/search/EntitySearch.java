package cat.xes.service.search;

import cat.xes.model.Association;
import cat.xes.model.EntityState;
import cat.xes.model.LegalForm;
import cat.xes.model.Sector;

public class EntitySearch {

	private EntityState state;
	private LegalForm legalForm;
	private Association association;
	private Sector sector;

	public EntityState getState() {
		return this.state;
	}

	public void setState(EntityState state) {
		this.state = state;
	}

	public LegalForm getLegalForm() {
		return this.legalForm;
	}

	public void setLegalForm(LegalForm legalForm) {
		this.legalForm = legalForm;
	}

	public Association getAssociation() {
		return this.association;
	}

	public void setAssociation(Association association) {
		this.association = association;
	}

	public Sector getSector() {
		return this.sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

}
